/**
 * Created by Dmitriy on 08.03.14.
 */

var crypto = require('crypto');

var mongoose = require('../libs/mongoose'),
    Schema = mongoose.Schema;

var schema = new Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    mail: {
        type: String,
        unique: true,
        required: true
    },
    created: {
        type: Date,
        default: Date.now
    },
    photo: {
        type: String,
        default: '\/public\/images\/guestImage.gif'
    },
    hashedPassword: {
        type: String,
        required: true
    },
    salt: {
        type: String,
        required: true
    }

});

schema.methods.encryptPassword = function(password) {
    return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
};

schema.virtual('password')
    .set(function(password) {
        this._plainPassword = password;
        this.salt = Math.random() + '';
        this.hashedPassword = this.encryptPassword(password);
    })
    .get(function() { return this._plainPassword; });


schema.methods.checkPassword = function(password) {
    return this.encryptPassword(password) === this.hashedPassword;
};

schema.methods.toSimpleObject = function() {
    return {
        firstName : this.get('firstName'),
        lastName : this.get('lastName'),
        mail : this.get('mail'),
        id : this.get('id'),
        photo : this.get('photo'),
        created : this.get('created')
    }
};

exports.User = mongoose.model('User', schema);