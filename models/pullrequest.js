/**
 * Created by Dmitriy on 19.03.14.
 */


var mongoose = require('../libs/mongoose'),
    Schema = mongoose.Schema;

var schema = new Schema({
    body: {
        type: String
    },
    presentationFromId : {
        type: String
    },
    presentationToId : {
        type: String
    },
    userToId : {
        type: String
    },
    userFromId : {
        type : String
    },
    userFromName : {
        type: String
    },
    presentationToName : {
        type: String
    }
});

schema.methods.toSimpleObject = function() {
    return {
        body: this.get('body'),
        presentationFromId: this.get('presentationFromId'),
        presentationToId: this.get('presentationToId'),
        userToId: this.get('userToId'),
        userFromId : this.get('userFromId'),
        userFromName: this.get('userFromName'),
        presentationToName: this.get('presentationToName'),
    }
};


exports.PullRequest = mongoose.model('PullRequest', schema);