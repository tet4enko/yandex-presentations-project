/**
 * Created by Dmitriy on 08.03.14.
 */

/**
 * Created by Dmitriy on 07.03.14.
 */

var DOMElements = {
    firstNameInput : document.getElementById('first-n'),
    lastNameInput : document.getElementById('last-n'),
    mailInput : document.getElementById('email'),
    passInput : document.getElementById('password'),
    confirmPassInput : document.getElementById('confirm-password'),

    firstNameError : document.getElementsByClassName('form-row first-name')[0].getElementsByClassName('error')[0],
    lastNameError : document.getElementsByClassName('form-row last-name')[0].getElementsByClassName('error')[0],
    mailError : document.getElementsByClassName('form-row email')[0].getElementsByClassName('error')[0],
    passError : document.getElementsByClassName('form-row password')[0].getElementsByClassName('error')[0],
    confirmPassError : document.getElementsByClassName('form-row confirm-password')[0].getElementsByClassName('error')[0],

    submitButton : document.getElementsByClassName('submit')[0],
    tryButton : document.getElementsByClassName('try')[0],
    loginButton : document.getElementsByClassName('sign-in')[0],

    dublicateError : document.getElementsByClassName('global-error red')[0]
}

var DOMMethods = {

    showFirstNameError : function()
    {
        DOMElements.firstNameError.innerHTML = '(Enter a not-empty First name)';
        DOMElements.firstNameInput.className = 'error';
    },

    hideFirstNameError : function()
    {
        DOMElements.firstNameError.innerHTML = '';
        DOMElements.firstNameInput.className = 'success';
    },

    showLastNameError : function()
    {
        DOMElements.lastNameError.innerHTML = '(Enter a not-empty Last name)';
        DOMElements.lastNameInput.className = 'error';
    },

    hideLastNameError : function()
    {
        DOMElements.lastNameError.innerHTML = '';
        DOMElements.lastNameInput.className = 'success';
    },

    showMailError : function()
    {
        DOMElements.mailError.innerHTML = '(Enter a valid e-mail)';
        DOMElements.mailInput.className = 'error';
    },

    hideMailError : function()
    {
        DOMElements.mailError.innerHTML = '';
        DOMElements.mailInput.className = 'success';
    },

    showPasswordError : function()
    {
        DOMElements.passError.innerHTML = '(Too simple password)';
        DOMElements.passInput.className = 'error';
    },

    hidePasswordError : function()
    {
        DOMElements.passError.innerHTML = '';
        DOMElements.passInput.className = 'success';
    },

    showConfirmPasswordError : function()
    {
        DOMElements.confirmPassError.innerHTML = '(Passwords are not equal)';
        DOMElements.confirmPassInput.className = 'error';
    },

    hideConfirmPasswordError : function()
    {
        DOMElements.confirmPassError.innerHTML = '';
        DOMElements.confirmPassInput.className = 'success';
    },

    showDublicateError : function()
    {
        DOMElements.dublicateError.innerHTML = 'User already exist!';
    },

    submitForm : function()
    {
        var flag = 1;
        if (DOMElements.firstNameInput.value.length)
        {
            DOMMethods.hideFirstNameError();
        }
        else
        {
            DOMMethods.showFirstNameError();
            flag = 0;
        }

        if (DOMElements.lastNameInput.value.length)
        {
            DOMMethods.hideLastNameError();
        }
        else
        {
            DOMMethods.showLastNameError();
            flag = 0;
        }

        if (!validationObject.checkMail(DOMElements.mailInput.value))
        {
            DOMMethods.showMailError();
            flag = 0;
        }
        else
        {
            DOMMethods.hideMailError();
        }

        if (!validationObject.checkPass(DOMElements.passInput.value))
        {
            DOMMethods.showPasswordError();
            flag = 0;
        }
        else
        {
            DOMMethods.hidePasswordError();
        }

        if (DOMElements.passInput.value === DOMElements.confirmPassInput.value)
        {
            DOMMethods.hideConfirmPasswordError();
        }
        else
        {
            DOMMethods.showConfirmPasswordError();
            flag = 0;
        }

        if (flag === 1)
        {
            //send request

            requestController.sendRequest('POST', window.location.pathname, 'data='+JSON.stringify({
                firstName : DOMElements.firstNameInput.value,
                lastName : DOMElements.lastNameInput.value,
                email : DOMElements.mailInput.value,
                password : DOMElements.passInput.value
            }), function(e) {
                    if (e.target.readyState != 4) return;
                    if (e.target.responseText === 'dublicate')
                    {
                        DOMMethods.showDublicateError();
                    }
                    else
                    {
                        window.location.href = '/user/' + e.target.responseText;
                    }
                });
        }
    },

    createPublicPresentation: function()
    {
        requestController.sendRequest('POST', '/trynowpresentation', 'data=createUserPres', function(e) {
            if (e.target.readyState != 4) return;
            window.location.href = '/presentation/' + e.target.responseText;
        });
    }
}

var validationObject = {

    mailRegexp : /^[-a-z0-9!#$%&'*+/=?^_`{|}~]+(?:\.[-a-z0-9!#$%&'*+/=?^_`{|}~]+)*@(?:[a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\.)*(?:aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])$/,

    checkMail : function(value)
    {
        if (value.match(this.mailRegexp))
        {
            return true;
        }
        return false;
    },

    checkPass : function(value)
    {
        return (value.length > 5);
    }

}


DOMElements.firstNameInput.addEventListener('keyup',function(e)
{
    if (e.target.value.length)
    {
        DOMMethods.hideFirstNameError();
    }
    else
    {
        DOMMethods.showFirstNameError();
    }
});

DOMElements.lastNameInput.addEventListener('keyup',function(e)
{
    if (e.target.value.length)
    {
        DOMMethods.hideLastNameError();
    }
    else
    {
        DOMMethods.showLastNameError();
    }
});


DOMElements.mailInput.addEventListener('keyup',function(e)
{
    if (validationObject.checkMail(e.target.value))
    {
        DOMMethods.hideMailError();
    }
    else
    {
        DOMMethods.showMailError();
    }
});



DOMElements.passInput.addEventListener('keyup',function(e)
{
    if (validationObject.checkPass(e.target.value))
    {
        DOMMethods.hidePasswordError();
    }
    else
    {
        DOMMethods.showPasswordError();
    }
});

DOMElements.confirmPassInput.addEventListener('keyup',function(e)
{
    if (e.target.value === DOMElements.passInput.value)
    {
        DOMMethods.hideConfirmPasswordError();
    }
    else
    {
        DOMMethods.showConfirmPasswordError();
    }
});


document.body.addEventListener('click',function(event)
{
    if (event.target.className === 'btn submit') DOMMethods.submitForm();
    if (event.target.className === 'btn sign-in') window.location.href = '/';
    if (event.target.className === 'btn btn-ok try') DOMMethods.createPublicPresentation();
});

document.body.addEventListener('keyup', function(event){
    if (event.keyCode === 13)
    {
        DOMMethods.submitForm();
    }
});


