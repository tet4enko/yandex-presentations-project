/*!
 * raphaeljs.serialize
 *
 * Copyright (c) 2010 Jonathan Spies
 * Licensed under the MIT license:
 * (http://www.opensource.org/licenses/mit-license.php)
 *
 */

Raphael.fn.serialize = {
    paper: this,

    json: function(paper) {
        var svgdata = [];

        for(var node = paper.bottom; node != null; node = node.next) {
            if (node && node.type) {
                switch(node.type) {
                    case "image":
                        var object = {
                            type: node.type,
                            width: node.attrs['width'],
                            height: node.attrs['height'],
                            x: Math.ceil(node.attrs['x']),
                            y: Math.ceil(node.attrs['y']),
                            src: node.attrs['src'],
                            transform: node.transformations ? node.transformations.join(' ') : '',
                            id : node.id,
                            borderId : node.borderId,
                            closerId : node.closerId,
                            resizerId : node.resizerId
                        }
                        break;
                    case "ellipse":
                        var object = {
                            type: node.type,
                            rx: node.attrs['rx'],
                            ry: node.attrs['ry'],
                            cx: node.attrs['cx'],
                            cy: node.attrs['cy'],
                            stroke: node.attrs['stroke'] === 0 ? 'none': node.attrs['stroke'],
                            'stroke-width': node.attrs['stroke-width'],
                            fill: node.attrs['fill'],
                            id : node.id
                        }
                        break;
                    case "rect":
                        var object = {
                            type: node.type,
                            x: Math.ceil(node.attrs['x']),
                            y: Math.ceil(node.attrs['y']),
                            width: node.attrs['width'],
                            height: node.attrs['height'],
                            stroke: node.attrs['stroke'] === 0 ? 'none': node.attrs['stroke'],
                            'stroke-width': node.attrs['stroke-width'],
                            fill: node.attrs['fill'],
                            id : node.id,
                            cursor: node.attrs['cursor']
                        }
                        break;
                    case "text":
                        var object = {
                            type: node.type,
                            font: node.attrs['font'],
                            'text-decoration': node.attrs['text-decoration'],
                            'font-family': node.attrs['font-family'],
                            'font-weight': node.attrs['font-weight'],
                            'font-style': node.attrs['font-style'],
                            'font-size': node.attrs['font-size'],
                            stroke: node.attrs['stroke'] === 0 ? 'none': node.attrs['stroke'],
                            fill: node.attrs['fill'] === 0 ? 'none' : node.attrs['fill'],
                            'stroke-width': node.attrs['stroke-width'],
                            x: Math.ceil(node.attrs['x']),
                            y: Math.ceil(node.attrs['y']),
                            borderId : node.borderId,
                            closerId : node.closerId,
                            text: node.attrs['text'],
                            'text-anchor': node.attrs['text-anchor'],
                            id : node.id
                        }
                        break;

                    case "path":
                        /*var path = "";

                        if(node.attrs['path'].constructor != Array){
                            path += node.attrs['path'];
                        }
                        else{
                            $.each(node.attrs['path'], function(i, group) {
                                $.each(group,
                                    function(index, value) {
                                        if (index < 1) {
                                            path += value;
                                        } else {
                                            if (index == (group.length - 1)) {
                                                path += value;
                                            } else {
                                                path += value + ',';
                                            }
                                        }
                                    });
                            });
                        }

                        var object = {
                            type: node.type,
                            fill: node.attrs['fill'],
                            opacity: node.attrs['opacity'],
                            translation: node.attrs['translation'],
                            scale: node.attrs['scale'],
                            path: path,
                            stroke: node.attrs['stroke'] === 0 ? 'none': node.attrs['stroke'],
                            'stroke-width': node.attrs['stroke-width'],
                            transform: node.transformations ? node.transformations.join(' ') : '',
                            id : node.id
                        }*/
                }

                if (object) {
                    svgdata.push(object);
                }
            }
        }

        return(JSON.stringify(svgdata));
    },

    load_json : function(json,paper) {
        if (typeof(json) == "string") { json = JSON.parse(json); } // allow stringified or object input

        var set = paper.set();
        json.forEach(function(node, index){
            try {

                var el = paper[node.type]().attr(node);

                el.borderId = node.borderId;
                el.closerId = node.closerId;
                el.resizerId = node.resizerId;
                el.id = node.id
                set.push(el);
            } catch(e) {}
        });

        return set;
    }
}
