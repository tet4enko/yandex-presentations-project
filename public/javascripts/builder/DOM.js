/**
 * Created by Dmitriy on 21.02.14.
 */
var DOMElements = {
    fakeSVGWrapper : document.getElementById('fake_wrap'),
    SVGWrapper : document.getElementById('wrap'),
    playArea : document.getElementsByClassName('play-area')[0],
    editMainBlock : document.getElementsByClassName('property-slide main-block')[0],
    editBackgroundBlock : document.getElementsByClassName('property-slide edit-background-block')[0],
    editTextBlock : document.getElementsByClassName('property-slide edit-text-block')[0],
    editPresentationBlock: document.getElementsByClassName('property-slide edit-presentation-block')[0],
    editImageBlock: document.getElementsByClassName('property-slide edit-image-block')[0],
    editTextInput : document.getElementsByClassName('edit-text-area')[0],
    playNavBar : document.getElementsByClassName('play-navbar')[0],
    playOverlay : document.getElementsByClassName('play-overlay')[0],
    playBarNext : document.getElementsByClassName('play-navbar-item next')[0],
    playBarPrev : document.getElementsByClassName('play-navbar-item prev')[0],
    playBarFull : document.getElementsByClassName('play-navbar-item full')[0],
    playBarPlay : document.getElementsByClassName('play-navbar-item play')[0],
    presName : document.getElementsByClassName('header-name header-font')[0],
    presNamePopupOverlay : document.getElementsByClassName('popup-overlay presentation-name')[0],
    presNamePopupCancelButton :  document.getElementsByClassName('popup-overlay presentation-name')[0].getElementsByClassName('btn popup-btn')[0],
    presNamePopupClose :  document.getElementsByClassName('popup-overlay presentation-name')[0].getElementsByClassName('popup-close')[0],
    presNameInput : document.getElementsByClassName('popup-overlay presentation-name')[0].getElementsByClassName('popup-input')[0],
    switchAccessButton : document.getElementsByClassName('btn btn-ok access')[0],
    switchAccessIcon : document.getElementById('access-span'),
    slideSizePopupOverlay : document.getElementsByClassName('popup-overlay slide-size')[0],
    colorComboBoxForSlide : document.getElementsByClassName('color-combo-box slide')[0],
    slideBackgroundImageButton: document.getElementsByClassName('btn btn-open')[0],
    slideBackgroundImageInput: document.getElementsByClassName('slide-background-file')[0],
    setBackgroundToAll : document.getElementsByClassName('btn backgr-to-all')[0],
    resetBackgroundToDefault : document.getElementsByClassName('btn backgr-to-default')[0],
    fontFamilyComboBoxForItem: document.getElementsByClassName('font-family-combo-box item')[0],
    slideBackgroundImageFromURLInput: document.getElementsByClassName('add-image slide-background')[0].getElementsByClassName('from-url')[0],
    imageItemFileInput: document.getElementsByClassName('add-image item')[0].getElementsByClassName('add-image-file')[0],
    imageItemFromUrlInput: document.getElementsByClassName('add-image item')[0].getElementsByClassName('from-url')[0],
    presentationBackgroundColorPopup: document.getElementsByClassName('property-slide edit-presentation-block')[0].getElementsByClassName('color-combo-box presentation-backgr')[0].getElementsByClassName('colors-popup')[0],
    BackrgoundColorComboBoxForPresentation: document.getElementsByClassName('property-slide edit-presentation-block')[0].getElementsByClassName('color-combo-box presentation-backgr')[0],
    presentationBackgroundImageInput: document.getElementsByClassName('presentation-background-file')[0],
    presentationBackgroundImageFromURLInput: document.getElementsByClassName('add-image presentation-backgr')[0].getElementsByClassName('from-url')[0],
    saveButton: document.getElementsByClassName('btn save')[0],
    saveText: document.getElementsByClassName('save-text')[0],
    addImageButton: document.getElementsByClassName('add-item-block add-imageelem')[0],
    editBackgroundButton: document.getElementsByClassName('add-item-block edit-background')[0],
    editPresentationButton: document.getElementsByClassName('add-item-block edit-presentation')[0],
    navbarTimeoutId: ''

}

//var imagePaths = []

var UIVariables =
{
    PWStatus : 0,
    ElementFontFamilyPopupStatus : 0,
    ElementFontSizePopupStatus : 0,
    ElementFontColorPopupStatus : 0,
    BackgroundColorPopupStatus : 0,
    PresentationFontFamilyPopupStatus : 0,
    PresentationFontSizePopupStatus : 0,
    PresentationFontColorPopupStatus : 0,
    PresentationBackgroundColorPopupStatus : 0,
    PresentationNamePopupStatus : 0,
    pageX: '',
    pageY: '',
    cursorOnPlayBar: false
}


var DOMMethods = {
    isPrivate: function()
    {
        return DOMElements.switchAccessButton ? DOMElements.switchAccessButton.innerHTML.indexOf('private') >= 0 : 'false';
    },

    getDocumentDimensions: function(param) {
        var D = document,h;
        if (param !== 'play')
        {
            h = Math.min(
                Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
                Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
                Math.max(D.body.clientHeight, D.documentElement.clientHeight)
            );
        }
        else
        {
            h = Math.max(
                Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
                Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
                Math.max(D.body.clientHeight, D.documentElement.clientHeight)
            );
        }
        var w = Math.max(
            Math.max(D.body.scrollWidth, D.documentElement.scrollWidth),
            Math.max(D.body.offsetWidth, D.documentElement.offsetWidth),
            Math.max(D.body.clientWidth, D.documentElement.clientWidth)
        );
        return { width: w, height: h };
    },

    scaleCanvas: function(canv){

        var freeAreaWidth, freeAreaHeight,
            defaultHeight = presentation.defaultValues().canvasHeight;

        if (canv === 'play-area')
        {
            canv = document.getElementsByClassName('play-area')[0];
            if (!canv)
                return;
            var docDimensions = DOMMethods.getDocumentDimensions('play');
            freeAreaWidth = docDimensions.width;
            freeAreaHeight = docDimensions.height;
        }
        else
        {
            var docDimensions = DOMMethods.getDocumentDimensions();
            freeAreaWidth = docDimensions.width - 535;
            freeAreaHeight = docDimensions.height -115;
        }
        var k1,k2;
        if (defaultHeight === '450px') {
            k1 = 4; k2 = 3;
        }
        if (defaultHeight === '400px') {
            k1 = 16; k2 = 10;
        }
        if (defaultHeight === '360px') {
            k1 = 16; k2 = 9;
        }

        var a = freeAreaWidth/k1;
        var b = freeAreaHeight/k2;

        if (b>a) {
            canv.style.width = Math.round(a*k1) + 'px';
            canv.style.height = Math.round(a*k2) + 'px';
        }else
        {
            canv.style.width = Math.round(b*k1) + 'px';
            canv.style.height = Math.round(b*k2) + 'px';
        }

    },

    showElementEditor : function(element) {
        if (UIVariables.PWStatus === 0)
        {
            DOMElements.editMainBlock.style.zIndex = 1;
            DOMElements.editMainBlock.style.opacity = 0;
        } else
        if (UIVariables.PWStatus === 1)
        {
            DOMElements.editTextBlock.style.opacity = 0;
            DOMElements.editTextBlock.style.zIndex = 1;
        }
        else
        if (UIVariables.PWStatus === 2)
        {
            DOMElements.editImageBlock.style.opacity = 0;
            DOMElements.editImageBlock.style.zIndex = 1;
        }
        else
        if (UIVariables.PWStatus === 3)
        {
            DOMElements.editBackgroundBlock.style.opacity = 0;
            DOMElements.editBackgroundBlock.style.zIndex = 1;
        }
        else
        if (UIVariables.PWStatus === 4)
        {
            DOMElements.editPresentationBlock.style.opacity = 0;
            DOMElements.editPresentationBlock.style.zIndex = 1;
        }

        if (element && element.type === 'text')
        {
            DOMElements.editTextBlock.style.zIndex = 2;
            DOMElements.editTextBlock.style.opacity = 1;
            DOMElements.editTextBlock.getElementsByClassName('edit-text-area')[0].select();
            UIVariables.PWStatus = 1;
        }
        else
        {
            DOMElements.editImageBlock.style.zIndex = 2;
            DOMElements.editImageBlock.style.opacity = 1;
            UIVariables.PWStatus = 2;
        }

    },

    showMainEditor : function()
    {
        if (UIVariables.PWStatus === 1)
        {
            DOMElements.editTextBlock.style.opacity = 0;
            DOMElements.editTextBlock.style.zIndex = 1;
        }
        else
        if (UIVariables.PWStatus === 2)
        {
            DOMElements.editImageBlock.style.opacity = 0;
            DOMElements.editImageBlock.style.zIndex = 1;
        }
        else
        if (UIVariables.PWStatus === 3)
        {
            DOMElements.editBackgroundBlock.style.opacity = 0;
            DOMElements.editBackgroundBlock.style.zIndex = 1;
        }
        else
        if (UIVariables.PWStatus === 4)
        {
            DOMElements.editPresentationBlock.style.opacity = 0;
            DOMElements.editPresentationBlock.style.zIndex = 1;
        }

        DOMElements.editMainBlock.style.zIndex = 2;
        DOMElements.editMainBlock.style.opacity = 1;

        UIVariables.PWStatus = 0;
    },

    showBackgroundEditor : function()
    {
        if (UIVariables.PWStatus === 0)
        {
            DOMElements.editMainBlock.style.zIndex = 1;
            DOMElements.editMainBlock.style.opacity = 0;
        } else
        if (UIVariables.PWStatus === 1)
        {
            DOMElements.editTextBlock.style.opacity = 0;
            DOMElements.editTextBlock.style.zIndex = 1;
        }
        else
        if (UIVariables.PWStatus === 2)
        {
            DOMElements.editImageBlock.style.opacity = 0;
            DOMElements.editImageBlock.style.zIndex = 1;
        }
        else
        if (UIVariables.PWStatus === 3)
        {
            DOMElements.editBackgroundBlock.style.opacity = 0;
            DOMElements.editBackgroundBlock.style.zIndex = 1;
        }
        else
        if (UIVariables.PWStatus === 4)
        {
            DOMElements.editPresentationBlock.style.opacity = 0;
            DOMElements.editPresentationBlock.style.zIndex = 1;
        }

        DOMElements.editBackgroundBlock.style.zIndex = 2;
        DOMElements.editBackgroundBlock.style.opacity = 1;

        UIVariables.PWStatus = 3;


    },

    showPresentationEditor : function()
    {

        if (UIVariables.PWStatus === 0)
        {
            DOMElements.editMainBlock.style.zIndex = 1;
            DOMElements.editMainBlock.style.opacity = 0;
        } else
        if (UIVariables.PWStatus === 1)
        {
            DOMElements.editTextBlock.style.opacity = 0;
            DOMElements.editTextBlock.style.zIndex = 1;
        }
        else
        if (UIVariables.PWStatus === 2)
        {
            DOMElements.editImageBlock.style.opacity = 0;
            DOMElements.editImageBlock.style.zIndex = 1;
        }
        else
        if (UIVariables.PWStatus === 3)
        {
            DOMElements.editBackgroundBlock.style.opacity = 0;
            DOMElements.editBackgroundBlock.style.zIndex = 1;
        }
        else
        if (UIVariables.PWStatus === 4)
        {
            DOMElements.editPresentationBlock.style.opacity = 0;
            DOMElements.editPresentationBlock.style.zIndex = 1;
        }

        DOMElements.editPresentationBlock.style.zIndex = 2;
        DOMElements.editPresentationBlock.style.opacity = 1;

        UIVariables.PWStatus = 4;
    },

    suppFullScreen: function()
    {
        return document.body.requestFullscreen || document.body.webkitRequestFullscreen || document.body.mozRequestFullScreen;
    },

    fullScreen : function(element)
    {
        if (element.requestFullscreen) {
            element.requestFullscreen();
        } else if (element.webkitRequestFullscreen) {
            element.webkitRequestFullscreen();
        } else if (element.mozRequestFullScreen) {
            element.mozRequestFullScreen();
        }
    },

    fullScreenCancel: function()
    {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        }
    },

    isFullscreen : function()
    {
    return document.fullScreen || document.webkitIsFullScreen || document.mozFullScreen;
    },

    showPresentationNamePopup: function(e)
    {
        DOMElements.presNamePopupOverlay.style.display = "block";
        DOMElements.presNameInput.value = e.target.innerHTML;
        DOMElements.presNameInput.select();
        UIVariables.PresentationNamePopupStatus = 1;
    },

    renamePresentation: function()
    {
        DOMElements.presName.innerHTML = DOMElements.presNameInput.value;
        DOMMethods.hidePresentationNamePopup();
    },

    hidePresentationNamePopup: function()
    {
        DOMElements.presNamePopupOverlay.style.display = "none";
        UIVariables.PresentationNamePopupStatus = 0;
    },

    switchAccess: function()
    {

        if (DOMElements.switchAccessIcon.className.indexOf('private') != -1)
        {
            DOMElements.switchAccessButton.innerHTML = DOMElements.switchAccessButton.innerHTML.replace(new RegExp('private','g'),'public');
            DOMElements.switchAccessIcon.className = "btn-img btn-access access-public";
        }
            else
        {
            DOMElements.switchAccessButton.innerHTML = DOMElements.switchAccessButton.innerHTML.replace(new RegExp('public','g'),'private');
            DOMElements.switchAccessIcon.className = "btn-img btn-access access-private";
        }
    },

    showSlideSizePopup: function()
    {
        DOMElements.slideSizePopupOverlay.style.display = "block";
    },

    hideSlideSizePopup: function(mode)
    {
        DOMElements.slideSizePopupOverlay.style.display = "none";
        if (mode == 0) {
            presentation.defaultValues().canvasWidth = '600px';
            presentation.defaultValues().canvasHeight = '450px';
        }
        if (mode == 1) {
            presentation.defaultValues().canvasWidth = '640px';
            presentation.defaultValues().canvasHeight = '400px';
        }
        if (mode == 2) {
            presentation.defaultValues().canvasWidth = '640px';
            presentation.defaultValues().canvasHeight = '360px';
        }
        presentation.setViewBox(presentation.paper);
        presentation.setViewBox(presentation.fakePaper);

        presentation.slideStack.push(new presentation.Slide());

        DOMMethods.scaleCanvas(DOMElements.SVGWrapper);
        DOMMethods.scaleCanvas('play-area');

        presentation.goToSlide(0, true);
    },

    toogleSlideBackgroundColorPopup: function()
    {
        var popup = document.getElementsByClassName('property-slide edit-background-block')[0].getElementsByClassName('colors-popup')[0];
        if (popup.className.indexOf('showed')>=0)
        {
            popup.className = "hided-colorpopup colors-popup";
            UIVariables.BackgroundColorPopupStatus = 0;
        }
        else
        {
            popup.className = "showed-colorpopup colors-popup";
            UIVariables.BackgroundColorPopupStatus = 1;
        }

    },

    hideSlideBackgroundColorPopup: function()
    {
        var popup = document.getElementsByClassName('property-slide edit-background-block')[0].getElementsByClassName('colors-popup')[0];
        popup.className = "hided-colorpopup colors-popup";
        UIVariables.BackgroundColorPopupStatus = 0;
    },

    tooglePresentationBackgroundColorPopup: function()
    {
        if (DOMElements.presentationBackgroundColorPopup.className.indexOf('showed')>=0)
        {
            DOMElements.presentationBackgroundColorPopup.className = "hided-colorpopup colors-popup";
            UIVariables.PresentationBackgroundColorPopupStatus = 0;
        }
        else
        {
            DOMElements.presentationBackgroundColorPopup.className = "showed-colorpopup colors-popup";
            UIVariables.PresentationBackgroundColorPopupStatus = 1;
        }

    },

    hidePresentationBackgroundColorPopup: function()
    {
        DOMElements.presentationBackgroundColorPopup.className = "hided-colorpopup colors-popup";
        UIVariables.PresentationBackgroundColorPopupStatus = 0;
    },

    openSlideBackgroundInput: function()
    {
        DOMElements.slideBackgroundImageInput.click();
    },

    openPresentationBackgroundInput: function()
    {
        DOMElements.presentationBackgroundImageInput.click();
    },

    toogleItemFontFamilyPopup: function()
    {
        var popup = document.getElementsByClassName('property-slide edit-text-block')[0].getElementsByClassName('fontfamily-popup')[0];
        if (popup.className.indexOf('showed')>=0)
        {
            popup.className = "fontfamily-popup hided-fontfamilypopup";
            UIVariables.ElementFontFamilyPopupStatus = 0;
        }
        else
        {
            popup.className =  "fontfamily-popup showed-fontfamilypopup";
            UIVariables.ElementFontFamilyPopupStatus = 1;
        }


    },

    hideItemFontFamilyPopup: function()
    {
        var popup = document.getElementsByClassName('property-slide edit-text-block')[0].getElementsByClassName('fontfamily-popup')[0];
        if (popup)
        {
            popup.className = "fontfamily-popup hided-fontfamilypopup";
            UIVariables.ElementFontFamilyPopupStatus = 0;
        }
    },

    tooglePresentationFontFamilyPopup: function()
    {
        var popup = document.getElementsByClassName('property-slide edit-presentation-block')[0].getElementsByClassName('fontfamily-popup')[0];
        if (popup.className.indexOf('showed')>=0)
        {
            popup.className = "fontfamily-popup hided-fontfamilypopup";
            UIVariables.PresentationFontFamilyPopupStatus = 0;
        }
        else
        {
            popup.className = "fontfamily-popup showed-fontfamilypopup";
            UIVariables.PresentationFontFamilyPopupStatus = 1;
        }

    },

    hidePresentationFontFamilyPopup: function()
    {
        var popup = document.getElementsByClassName('property-slide edit-presentation-block')[0].getElementsByClassName('fontfamily-popup')[0];
        if (popup)
        {
            UIVariables.PresentationFontFamilyPopupStatus = 0;
            popup.className = "fontfamily-popup hided-fontfamilypopup";
        }
    },

    toogleItemFontSizePopup: function()
    {
        var popup = document.getElementsByClassName('property-slide edit-text-block')[0].getElementsByClassName('fontsize-popup')[0];
        if (popup.className.indexOf('showed')>=0)
        {
            popup.className = "fontsize-popup hidedPopup";
            UIVariables.ElementFontSizePopupStatus = 0;
        }
        else
        {
            popup.className = "fontsize-popup showedPopup";
            document.getElementsByClassName('font-size-preview item')[0].select();
            UIVariables.ElementFontSizePopupStatus = 1;
        }
    },

    hideItemFontSizePopup: function()
    {

        var popup = document.getElementsByClassName('property-slide edit-text-block')[0].getElementsByClassName('fontsize-popup')[0];
        if (popup)
        {
            UIVariables.ElementFontSizePopupStatus = 0;
            popup.className = "fontsize-popup hidedPopup";
        }
    },


    tooglePresentationFontSizePopup: function()
    {
        var popup = document.getElementsByClassName('property-slide edit-presentation-block')[0].getElementsByClassName('fontsize-popup')[0];
        if (popup.className.indexOf('showed')>=0)
        {
            popup.className = "fontsize-popup hidedPopup";
            UIVariables.PresentationFontSizePopupStatus = 0;
        }
        else
        {
            popup.className = "fontsize-popup showedPopup";
            document.getElementsByClassName('font-size-preview presentation')[0].select();
            UIVariables.PresentationFontSizePopupStatus = 1;
        }
    },

    hidePresentationFontSizePopup: function()
    {

        var popup = document.getElementsByClassName('property-slide edit-presentation-block')[0].getElementsByClassName('fontsize-popup')[0];
        if (popup)
        {
            UIVariables.PresentationFontSizePopupStatus = 0;
            popup.className = "fontsize-popup hidedPopup";
        }
    },

    getFontSizeForItemValue: function()
    {
        return document.getElementsByClassName('font-size-combo-box item')[0].getElementsByClassName('font-size-preview item')[0].value;
    },

    getFontSizeForPresentationValue: function()
    {
        return document.getElementsByClassName('font-size-preview presentation')[0].value;
    },

    hideItemColorPopup: function()
    {
        var popup = document.getElementsByClassName('property-slide edit-text-block')[0].getElementsByClassName('colors-popup')[0];
        if (popup)
        {
            popup.className = "hided-colorpopup colors-popup";
            UIVariables.ElementFontColorPopupStatus = 0;
        }
    },

    toogleItemColorPopup: function()
    {

        var popup = document.getElementsByClassName('property-slide edit-text-block')[0].getElementsByClassName('colors-popup')[0];
        if (popup.className.indexOf('showed')>=0)
        {
            popup.className = "hided-colorpopup colors-popup";
            UIVariables.ElementFontColorPopupStatus = 0;
        }
        else
        {
            popup.className = "showed-colorpopup colors-popup";
            UIVariables.ElementFontColorPopupStatus = 1;
        }

    },

    hidePresentationColorPopup: function()
    {

        var popup = document.getElementsByClassName('property-slide edit-presentation-block')[0].getElementsByClassName('colors-popup')[0];
        if (popup)
        {
            popup.className = "hided-colorpopup colors-popup";
            UIVariables.PresentationFontColorPopupStatus = 0;
        }
    },

    tooglePresentationColorPopup: function()
    {
        var popup = document.getElementsByClassName('property-slide edit-presentation-block')[0].getElementsByClassName('colors-popup')[0];
        if ( popup.className.indexOf('showed')>=0)
        {
            popup.className = "hided-colorpopup colors-popup";
            UIVariables.PresentationFontColorPopupStatus = 0;
        }
        else
        {
            popup.className = "showed-colorpopup colors-popup";
            UIVariables.PresentationFontColorPopupStatus = 1;
        }

    },

    getItemBoldValue: function()
    {
        return document.getElementById('bold item').checked;
    },

    getItemItalicValue: function()
    {
        return document.getElementById('italic item').checked;
    },

    getItemUnderlineValue: function()
    {
        return document.getElementById('underline item').checked;
    },

    setSlideBackgroundImageFromURL: function()
    {
        presentation.setSlideBackgroundImage(DOMElements.slideBackgroundImageFromURLInput.value);
        DOMElements.slideBackgroundImageFromURLInput.value = "";
    },

    setPresentationBackgroundImageFromURL: function()
    {
        presentation.setPresentationBackgroundImage(DOMElements.presentationBackgroundImageFromURLInput.value);
        DOMElements.presentationBackgroundImageFromURLInput.value = "";
    },

    openImageItemInput: function()
    {
        DOMElements.imageItemFileInput.click();
    },

    createImageItemFromURL: function()
    {
        presentation.addImage(DOMElements.imageItemFromUrlInput.value);
        DOMElements.imageItemFromUrlInput.value = "";
    },

    setToFullScreenOnPlayBar: function()
    {
        DOMElements.playBarFull.className = "play-navbar-item full";
    },

    setToNotFullScreenOnPlayBar: function()
    {
        DOMElements.playBarFull.className = 'play-navbar-item not-full';
    },

    setPlayBarPrevToEnabled: function()
    {
        DOMElements.playBarPrev.className = "play-navbar-item prev";
    },

    setPlayBarPrevToNotEnabled: function()
    {
        DOMElements.playBarPrev.className = "play-navbar-item prev not-enabled";
    },

    setPlayBarNextToEnabled: function()
    {
        DOMElements.playBarNext.className = "play-navbar-item next";
    },

    setPlayBarNextToNotEnabled: function()
    {
        DOMElements.playBarNext.className = "play-navbar-item next not-enabled";
    },

    setPlayBarPlayToEnabled: function()
    {
        if (DOMElements.playBarPlay.className.indexOf('not-enabled')>=0)
        {
            DOMElements.playBarPlay.className =DOMElements.playBarPlay.className.replace('not-enabled','');
        }
    },

    setPlayBarPlayToNotEnabled: function()
    {
        if (DOMElements.playBarPlay.className.indexOf('not-enabled') === -1)
        {
        DOMElements.playBarPlay.className = DOMElements.playBarPlay.className + ' not-enabled';
        }
    },

    setPlayBarPlayToPlayed: function()
    {
        DOMElements.playBarPlay.className = DOMElements.playBarPlay.className.replace('stopped','played');
    },

    setPlayBarPlayToStopped: function()
    {
        DOMElements.playBarPlay.className = DOMElements.playBarPlay.className.replace('played','stopped');
    },

    showPlayOverlay: function()
    {
        DOMElements.playOverlay.style.zIndex = 5;
        DOMElements.playOverlay.style.opacity = 1;
    },

    hidePlayOverlay: function()
    {
        DOMElements.playOverlay.style.zIndex = -5;
        DOMElements.playOverlay.style.opacity = 0;
    },


    makePlayNavBarInvisible: function()
    {
        if (UIVariables.cursorOnPlayBar)
        {
            if (DOMElements.navbarTimeoutId)
            {
                clearTimeout(DOMElements.navbarTimeoutId);

            }
            DOMElements.navbarTimeoutId = setTimeout(DOMMethods.makePlayNavBarInvisible, 1500);
        }
        else
        if (DOMElements.playNavBar.className.indexOf('invisible') === -1)
        {
            DOMElements.playNavBar.className = DOMElements.playNavBar.className.replace('visible','invisible');
        }

    },

    makePlayNavBarVisible: function()
    {

        DOMElements.playNavBar.className = DOMElements.playNavBar.className.replace('in','');
        if (DOMElements.navbarTimeoutId)
        {
            clearTimeout(DOMElements.navbarTimeoutId);

        }
        DOMElements.navbarTimeoutId = setTimeout(DOMMethods.makePlayNavBarInvisible, 1500);
    },

    sendSaveRequest: function(data)
    {
        requestController.sendRequest('POST', window.location.pathname, 'name=' + data.name + '&private=' + data.private + '&preview=' + encodeURIComponent(data.preview) + '&body=' + JSON.stringify([data.defaultValues,data.slideStack]),
            function(e) {
                if (e.target.readyState != 4) return;
                if (e.target.responseText === 'success')
                {
                    DOMElements.saveText.innerHTML = 'Saved!';
                }
                else
                {
                    DOMElements.saveText.innerHTML = 'Error!';
                }
        });

    },

    setSaveButtonDefaultText: function()
    {
        if (DOMElements.saveText)
        {
            DOMElements.saveText.innerHTML = 'Save';
        }
    },

     unique: function(arr)
     {
        var obj = {};
        for(var i=0; i<arr.length; i++)
        {
            var str = arr[i];
            obj[str] = true; // запомнить строку в виде свойства объекта
        }
        return Object.keys(obj); // или собрать ключи перебором для IE<9
    },


    getOffset: function(elem) {
        if (elem.getBoundingClientRect) {
            // "правильный" вариант
            return this.getOffsetRect(elem)
        } else {
            // пусть работает хоть как-то
            return this.getOffsetSum(elem)
        }
    },

    getOffsetSum: function(elem) {
        var top=0, left=0
        while(elem) {
            top = top + parseInt(elem.offsetTop)
            left = left + parseInt(elem.offsetLeft)
            elem = elem.offsetParent
        }

        return {top: top, left: left}
    },

    getOffsetRect: function(elem) {
        var box = elem.getBoundingClientRect()

        var body = document.body
        var docElem = document.documentElement

        var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop
        var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft

        var clientTop = docElem.clientTop || body.clientTop || 0
        var clientLeft = docElem.clientLeft || body.clientLeft || 0

        var top  = box.top +  scrollTop - clientTop
        var left = box.left + scrollLeft - clientLeft

        return { top: Math.round(top), left: Math.round(left) }
    },

    hideSeparator: function(index)
    {
        var separator = document.getElementsByClassName('separator')[index];
        if (separator)
        {
            separator.className =
                separator.className.indexOf('showed') >= 0 ? separator.className.replace(' showed','') : separator.className;
        }
    },

    showSeparator: function(index)
    {
        var separator = document.getElementsByClassName('separator')[index];
        if (separator)
        {
            separator.className =
                separator.className.indexOf('showed') >= 0 ? separator.className : separator.className + ' showed';
        }
    }

}


function isDescendant(parent, child) {
    var node = child.parentNode;
    while (node != null) {
        if (node == parent) {
            return true;
        }
        node = node.parentNode;
    }
    return false;
}


document.body.addEventListener('click',function(e){

    //presantation name popup
    if (e.target.className === "header-name header-font") DOMMethods.showPresentationNamePopup(e);
    if (e.target.className === "btn btn-ok popup-btn") DOMMethods.renamePresentation();
    if ((e.target.className === "popup-overlay presentation-name" || e.target.className === "btn popup-btn" || e.target.className === "popup-close") && (UIVariables.PresentationNamePopupStatus === 1))
        DOMMethods.hidePresentationNamePopup();

    //switch access button
    if (e.target.className === "btn btn-ok access" || isDescendant(DOMElements.switchAccessButton, e.target)) DOMMethods.switchAccess();

    //slide size popup
    if (e.target.className === "popup-close slide-size" || e.target.className === "popup-overlay slide-size" || e.target.className === "btn btn-ok slide-size mode0") DOMMethods.hideSlideSizePopup(0);
    if (e.target.className === "btn slide-size mode1") DOMMethods.hideSlideSizePopup(1);
    if (e.target.className === "btn slide-size mode2") DOMMethods.hideSlideSizePopup(2);

    //background slide color
    if (e.target.className === "color-combo-box slide" || e.target.className === "color-preview slide") DOMMethods.toogleSlideBackgroundColorPopup();
    if (!(e.target.className === "color-combo-box slide" || isDescendant(DOMElements.colorComboBoxForSlide, e.target)) && (UIVariables.BackgroundColorPopupStatus === 1)) DOMMethods.hideSlideBackgroundColorPopup();

    //background presentation color
    if (e.target.className === "color-combo-box presentation-backgr" || e.target.className === "color-preview presentation-backgr") DOMMethods.tooglePresentationBackgroundColorPopup();
    if (!(e.target.className === "color-combo-box presentation-backgr" || e.target.className === "color-preview presentation-backgr") && (UIVariables.PresentationBackgroundColorPopupStatus === 1)) DOMMethods.hidePresentationBackgroundColorPopup();


    //background slide image
    if (e.target.className === "btn-open slide-background") DOMMethods.openSlideBackgroundInput();
    if (e.target.className === "from-url-ok slide-background") DOMMethods.setSlideBackgroundImageFromURL();

    //background presentation image
    if (e.target.className === "btn-open presentation-backgr") DOMMethods.openPresentationBackgroundInput();
    if (e.target.className === "from-url-ok presentation-backgr") DOMMethods.setPresentationBackgroundImageFromURL();

    //set backround slide to All
    if (e.target.className === "btn backgr-to-all") presentation.setCurrentBackgroundToAll();

    //reset slide background to Default
    if (e.target.className === "btn backgr-to-default") presentation.resetSlideBackgroundToDefault();

    //item font-family
    if (e.target.className === "font-family-combo-box item" || e.target.className === "font-family-preview item") DOMMethods.toogleItemFontFamilyPopup();
    if (!(e.target.className === "font-family-combo-box item" || e.target.className === "font-family-preview item") && (UIVariables.ElementFontFamilyPopupStatus === 1)) DOMMethods.hideItemFontFamilyPopup();

    //item font-size
    if (e.target.className === "font-size-combo-box item" || e.target.className === "font-size-preview item") DOMMethods.toogleItemFontSizePopup();
    if (!(e.target.className === "font-size-combo-box item" || e.target.className === "font-size-preview item") && (UIVariables.ElementFontSizePopupStatus === 1)) DOMMethods.hideItemFontSizePopup();

    //item font color
    if (e.target.className === "color-combo-box item" || e.target.className === "color-preview item") DOMMethods.toogleItemColorPopup();
    if (!(e.target.className === "color-combo-box item" ||  e.target.className === "color-preview item" || e.target.className === "color-items") && (UIVariables.ElementFontColorPopupStatus === 1)) DOMMethods.hideItemColorPopup();

    //presentation font-family
    if (e.target.className === "font-family-combo-box presentation" || e.target.className === "font-family-preview presentation") DOMMethods.tooglePresentationFontFamilyPopup();
    if (!(e.target.className === "font-family-combo-box presentation" || e.target.className === "font-family-preview presentation") && (UIVariables.PresentationFontFamilyPopupStatus === 1)) DOMMethods.hidePresentationFontFamilyPopup();

    //presentation font-size
    if (e.target.className === "font-size-combo-box presentation" || e.target.className === "font-size-preview presentation") DOMMethods.tooglePresentationFontSizePopup();
    if (!(e.target.className === "font-size-combo-box presentation" || e.target.className === "font-size-preview presentation") && (UIVariables.PresentationFontSizePopupStatus === 1)) DOMMethods.hidePresentationFontSizePopup();

    //presentation font color
    if (e.target.className === "color-combo-box presentation" || e.target.className === "color-preview presentation") DOMMethods.tooglePresentationColorPopup();
    if (!(e.target.className === "color-combo-box presentation" ||  e.target.className === "color-preview presentation") && (UIVariables.PresentationFontColorPopupStatus === 1)) DOMMethods.hidePresentationColorPopup();

    //image Item
    if (e.target.className === "btn-open item") DOMMethods.openImageItemInput();
    if (e.target.className === "from-url-ok item") DOMMethods.createImageItemFromURL();

    //save presentation
    if (e.target.className === "btn save" || isDescendant(DOMElements.saveButton, e.target)) presentation.savePresentation();

    //back to site
    if (e.target.className === "to-site") window.location.href = '/';

    //PW Navigate
    if (e.target.className === "add-item-block add-imageelem" || isDescendant(DOMElements.addImageButton, e.target)) DOMMethods.showElementEditor();
    if (e.target.className === "add-item-block edit-background" || isDescendant(DOMElements.editBackgroundButton, e.target)) DOMMethods.showBackgroundEditor();
    if (e.target.className === "add-item-block edit-presentation" || isDescendant(DOMElements.editPresentationButton, e.target)) DOMMethods.showPresentationEditor();
    if (e.target.className === "back-btn") DOMMethods.showMainEditor();


});

var slideMove = {
    isMoving: false,
    startSlideIndex: 0,
    endSlideIndex: 0,
    topMiddleOfPreviewImage: 0,
    separatorOn: 0
}

document.body.addEventListener('mousedown',function(e){
    var flag = 0, targetElem = e.target;
    var parent = targetElem.parentNode;
    while (parent && targetElem.className)
    {
        if (targetElem.className.indexOf && targetElem.className.indexOf('preview-image') >= 0)
        {
            flag = 1;
            break;
        }
        targetElem = targetElem.parentNode;
    }
    if (!flag) return;

    slideMove.isMoving = true;
    slideMove.startSlideIndex = parseInt(targetElem.parentNode.getElementsByClassName('preview-index')[0].innerHTML);
    slideMove.topMiddleOfPreviewImage = DOMMethods.getOffset(targetElem).top + 55;
});



window.addEventListener('resize',function(event){
    if (presentation.defaultValues().canvasHeight)
    {
        DOMMethods.scaleCanvas(DOMElements.SVGWrapper);
        DOMMethods.scaleCanvas('play-area');
    }
});

window.addEventListener('keydown',function(event){
    if (event.keyCode === 46){
        presentation.removeSelectedItem();
    };
    if (event.keyCode === 27){
        var a=DOMMethods.isFullscreen();

    };
    if ((playPresentation.playStatus() > 0) && (event.keyCode === 37 || event.keyCode === 40)){
        playPresentation.prev();
    };
    if ((playPresentation.playStatus() > 0) && (event.keyCode === 39 || event.keyCode === 38)){
        playPresentation.next();
    };

});



document.body.addEventListener('mousemove',function(event){

   if (playPresentation.playStatus()>0 && UIVariables.pageX !== event.pageX && UIVariables.pageY !== event.pageY)
   {
       DOMMethods.makePlayNavBarVisible();
       UIVariables.pageX = event.pageX;
       UIVariables.pageY = event.pageY;
   }

    if (slideMove.isMoving)
    {
        var newSeparatorIndex = (slideMove.startSlideIndex) + (Math.floor((event.pageY - slideMove.topMiddleOfPreviewImage) / 130));
        if (newSeparatorIndex !== slideMove.separatorOn)
        {
            DOMMethods.hideSeparator(slideMove.separatorOn);
            slideMove.separatorOn = newSeparatorIndex;
            DOMMethods.showSeparator(slideMove.separatorOn);
        }
    }


}.bind(this));


document.body.addEventListener('mouseup', function(e){

    if (slideMove.isMoving)
    {
            DOMMethods.hideSeparator(slideMove.separatorOn);
            slideMove.isMoving = false;
            var slideOffset = ((e.pageY-slideMove.topMiddleOfPreviewImage) >=0) ? Math.ceil((e.pageY-slideMove.topMiddleOfPreviewImage)/130) : Math.floor((e.pageY-slideMove.topMiddleOfPreviewImage)/130)
            presentation.moveSlide(slideMove.startSlideIndex, slideOffset);
    }
});


DOMElements.imageItemFileInput.addEventListener('change',function(e){
    if (e.target.files[0])
    {
        var formData = new FormData();
        formData.append("file", e.target.files[0]);

        var xhr = new XMLHttpRequest();

        xhr.open("POST", "/saveimage");

        xhr.onreadystatechange = function(e) {
            if (xhr.readyState != 4) return;
            presentation.addImage(e.target.responseText.replace("public\\images\\","/public/images/"));
        }

        xhr.send(formData);

        this.value = '';
    }

});


DOMElements.slideBackgroundImageInput.addEventListener('change',function(e){
    if (e.target.files[0])
    {
        var formData = new FormData();
        formData.append("file", e.target.files[0]);

        var xhr = new XMLHttpRequest();

        xhr.open("POST", "/saveimage");

        xhr.onreadystatechange = function(e) {
            if (xhr.readyState != 4) return;
            presentation.setSlideBackgroundImage(e.target.responseText.replace("public\\images\\","/public/images/"));
        }
        xhr.send(formData);

        this.value = '';
    }

});

DOMElements.presentationBackgroundImageInput.addEventListener('change', function(e){
    if (e.target.files[0])
    {
        var formData = new FormData();
        formData.append("file", e.target.files[0]);

        var xhr = new XMLHttpRequest();

        xhr.open("POST", "/saveimage");

        xhr.onreadystatechange = function(e) {
            if (xhr.readyState != 4) return;
            presentation.setPresentationBackgroundImage(e.target.responseText.replace("public\\images\\","/public/images/"));
        }

        xhr.send(formData);

        this.value = '';
    }

});

DOMElements.playNavBar.addEventListener('mouseover', function(event){
    UIVariables.cursorOnPlayBar = true;
});

DOMElements.playNavBar.addEventListener('mouseout', function(event){
    UIVariables.cursorOnPlayBar = false;
});

Array.prototype.move = function (old_index, new_index) {
    if (new_index >= this.length) {
        var k = new_index - this.length;
        while ((k--) + 1) {
            this.push(undefined);
        }
    }
    this.splice(new_index, 0, this.splice(old_index, 1)[0]);
    return this; // for testing purposes
};
