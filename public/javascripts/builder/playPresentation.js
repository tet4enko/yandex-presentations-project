
var playPresentation = (function(){

    var currentSlideIndex = ko.observable(0);
    var playStatus = ko.observable(0);
    var timeOutId = '';


    var start = function(){
        DOMMethods.fullScreen(document.documentElement);
        DOMMethods.showPlayOverlay();
        playPresentation.currentSlideIndex(0);
        DOMMethods.setPlayBarPlayToStopped();
        if (presentation.slideStack().length === 1)
        {
            DOMMethods.setPlayBarNextToNotEnabled();
            DOMMethods.setPlayBarPrevToNotEnabled();
            DOMMethods.setPlayBarPlayToNotEnabled();
        }
        else
        {
            DOMMethods.setPlayBarPrevToNotEnabled();
            DOMMethods.setPlayBarNextToEnabled();
            DOMMethods.setPlayBarPlayToEnabled();
        }

        DOMMethods.makePlayNavBarVisible();

        playPresentation.playStatus(1);

    }

    var prev = function(){

        if (playPresentation.currentSlideIndex())
        {
            playPresentation.currentSlideIndex(playPresentation.currentSlideIndex()-1);
        }

        if (playPresentation.currentSlideIndex() === 0)
        {
            DOMElements.playBarPrev.className = "play-navbar-item prev not-enabled";
        }

        if ((playPresentation.currentSlideIndex()+1) !== presentation.slideStack().length)
        {
            DOMElements.playBarNext.className = "play-navbar-item next";
        }

    }

    var next = function(){
        if ((playPresentation.currentSlideIndex()+1) < presentation.slideStack().length)
        {
            playPresentation.currentSlideIndex(playPresentation.currentSlideIndex()+1);
        }
        if ((playPresentation.currentSlideIndex()+1) === presentation.slideStack().length)
        {
            DOMElements.playBarNext.className = "play-navbar-item next not-enabled";
            if (playPresentation.playStatus() === 2)
            {
                playPresentation.play();
            }
        }
        if (playPresentation.currentSlideIndex() !== 0)
        {
            DOMElements.playBarPrev.className = "play-navbar-item prev";
        }
    }

    var close = function(){
        DOMMethods.hidePlayOverlay();
        playPresentation.playStatus(0)
        DOMMethods.fullScreenCancel();
    }

    var changeScreenMode = function(){
        if (DOMMethods.isFullscreen())
        {
            DOMMethods.setToNotFullScreenOnPlayBar();
            DOMMethods.fullScreenCancel();
        }
        else
        {
            DOMMethods.setToFullScreenOnPlayBar();
            DOMMethods.fullScreen(document.documentElement);
        }
    }

    var play = function()
    {
        if (presentation.slideStack().length === 1)
        {
            return;
        }
        if (playPresentation.playStatus() === 1)
        {
            playPresentation.playStatus(2);
            DOMMethods.setPlayBarPlayToPlayed();
            playPresentation.timeOutId = setInterval(playPresentation.next, 2000);
        }
        else  if (playPresentation.playStatus() === 2)
        {
            clearInterval(playPresentation.timeOutId);
            DOMMethods.setPlayBarPlayToStopped();
            playPresentation.playStatus(1);
        }
    }


    return {
        currentSlideIndex: currentSlideIndex,
        start: start,
        prev: prev,
        next: next,
        close: close,
        play: play,
        playStatus: playStatus,
        timeOutId: timeOutId,
        changeScreenMode: changeScreenMode
    };


})();


