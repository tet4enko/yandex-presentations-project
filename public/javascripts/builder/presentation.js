/**
 * Created by Dmitriy on 27.01.14.
 */

var presentation = (function(){

    var slideStack = ko.observableArray();//koobs array
    var currentSlide = ko.observable(0);
    var paper='';
    var fakePaper = '';
    var selectedItem = ko.observable('');

    var Background = function(){
        this.type = "color";
        this.value = "white"
    };

    var defaultValues = ko.observable({
        canvasWidth: '',
        canvasHeight: '',
        background: new Background(),
        fontFamily: 'Comic Sans MS',
        fontSize: '60px',
        fontColor: 'rgb(0,0,0)',
        fontWeight: 'normal',
        fontStyle: 'normal',
        textDecoration: 'none'
    });

    var defaultURLs = {
        closer: "url('../../images/newRem1.png')",
        resizer: "url('../../images/resizer1.png')"
    }

    var colorsForPopups = ['rgb(0,0,0)', 'rgb(67,67,67)', 'rgb(102,102,102)', 'rgb(153,153,153)', 'rgb(183,183,183)','rgb(204,204,204)', 'rgb(217,217,217)', 'rgb(239,239,239)','rgb(243, 243, 243)','rgb(255, 255, 255)',
                            'rgb(152,0,0)','rgb(255,0,0)','rgb(255,153,0)','rgb(255,255,0)','rgb(0,255,0)','rgb(0,255,255)','rgb(74,134,232)','rgb(0,0,255)','rgb(153,0,255)','rgb(255,0,255)',
                            'rgb(230,184,175)','rgb(244,204,204)','rgb(252,229,205)','rgb(255,242,204)','rgb(217,234,211)','rgb(208,224,227)','rgb(201,218,248)','rgb(207,226,243)','rgb(217,210,233)','rgb(234,209,220)',
                            'rgb(221, 126, 107)','rgb(234, 153, 153)','rgb(249, 203, 156)','rgb(255, 229, 153)','rgb(182, 215, 168)','rgb(162, 196, 201)','rgb(164, 194, 244)','rgb(159, 197, 232)','rgb(180, 167, 214)', 'rgb(213, 166, 189)',
                            'rgb(204, 65, 37)','rgb(224, 102, 102)','rgb(246, 178, 107)','rgb(255, 217, 102)','rgb(147, 196, 125)','rgb(118, 165, 175)','rgb(109, 158, 235)','rgb(111, 168, 220)','rgb(142, 124, 195)','rgb(194, 123, 160)',
                            'rgb(166, 28, 0)','rgb(204, 0, 0)','rgb(230, 145, 56)','rgb(241, 194, 50)','rgb(106, 168, 79)','rgb(69, 129, 142)','rgb(60, 120, 216)','rgb(61, 133, 198)','rgb(103, 78, 167)','rgb(166, 77, 121)',
                            'rgb(133, 32, 12)','rgb(153, 0, 0)','rgb(180, 95, 6)','rgb(191, 144, 0)','rgb(56, 118, 29)','rgb(19, 79, 92)','rgb(17, 85, 204)','rgb(11, 83, 148)','rgb(53, 28, 117)','rgb(116, 27, 71)',
                            'rgb(91, 15, 0)','rgb(102, 0, 0)','rgb(120, 63, 4)','rgb(127, 96, 0)','rgb(39, 78, 19)','rgb(12, 52, 61)','rgb(28, 69, 135)','rgb(7, 55, 99)','rgb(32, 18, 77)','rgb(116, 27, 71)'];

    var fontFamiliesForPopups = ['Arial', 'Comic Sans MS', 'Courier New', 'Georgia', 'Impact', 'Times New Roman', 'Trebuchet MS', 'Verdana'];

    var fontSizesForPopups = [12,14,18,24,30,36,48,60,72,96];


    var setDefaultBackground =function(paper){
        paper.clear();
        if (presentation.defaultValues().background.type === "color"){
            var rect = paper.rect(0, 0, parseInt(presentation.defaultValues().canvasWidth)+2, parseInt(presentation.defaultValues().canvasHeight)+2)
                .attr('fill',presentation.defaultValues().background.value)
                .attr('stroke-width',0);
            rect.click(presentation.unselectAll);
        }
        else
        {
            var image = paper.image(presentation.defaultValues().background.value,0, 0, parseInt(presentation.defaultValues().canvasWidth)+2, parseInt(presentation.defaultValues().canvasHeight)+2)
                .attr('stroke-width',0);
            image.click(presentation.unselectAll);
        }

    };

    var S4 = function() {
        return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
    }

    var Slide = function(inputData){

        if (!inputData)
        {
            presentation.setDefaultBackground(presentation.fakePaper);
            this.paperObject = ko.observable(presentation.fakePaper.serialize.json(presentation.fakePaper));
            this.id = (S4() + S4() + "-" + S4() + "-4" + S4().substr(0,3) + "-" + S4() + "-" + S4() + S4() + S4()).toLowerCase();
        }
        else
        {
            this.paperObject = ko.observable(inputData.paperObject);
            this.id = inputData.id;
        }

        this.previewImage = ko.computed(function() {

            presentation.fakePaper.clear();
            presentation.fakePaper.serialize.load_json(this.paperObject(), presentation.fakePaper);
            presentation.fakePaper.forEach(function(item){
                presentation.hideBorder(presentation.fakePaper, item);
                presentation.hideCloser(presentation.fakePaper, item);
                if (item.type === 'image')
                {
                    presentation.hideResizer(presentation.fakePaper,item);
                }
            });
            return DOMElements.fakeSVGWrapper.innerHTML;

        }, this);

    };



    var initialize = function(presentationData){
        presentation.paper = Raphael("wrap");
        presentation.fakePaper = Raphael("fake_wrap");

        if (!presentationData) //new presentation
        {
            DOMMethods.showSlideSizePopup();
        }
        else
        {

            var inputSlideStack = presentationData[1];
            var inputDefaultValues = presentationData[0];

            presentation.defaultValues(inputDefaultValues);

            presentation.setViewBox(presentation.paper);
            presentation.setViewBox(presentation.fakePaper);

            DOMMethods.scaleCanvas(DOMElements.SVGWrapper);
            DOMMethods.scaleCanvas('play-area');

            inputSlideStack.forEach(function(item){
                presentation.slideStack.push(new Slide(item));
            })

            presentation.goToSlide(0,true);
        }

    };

    var addSlide  = function(){
        var newSlide = new Slide();
        slideStack.splice(presentation.currentSlide()+1,0,newSlide);
        presentation.goToSlide(newSlide);
    }.bind(this);

    var removeSlide = function(index){

        if (typeof index !== 'number')
        {
            index = presentation.slideStack.indexOf(index);
        }


        var itCurrent = index === presentation.currentSlide();

        if (presentation.slideStack().length-1)
        {
            slideStack.splice(index, 1);
            if (itCurrent)
            {
                DOMMethods.showMainEditor();
                presentation.goToSlide(presentation.currentSlide() !== presentation.slideStack().length ? presentation.currentSlide() : presentation.currentSlide()-1,true);
            }
            else if (index < presentation.currentSlide())
            {
                presentation.currentSlide(presentation.currentSlide()-1);
            }

        }
        else
        {
            presentation.setDefaultBackground(presentation.paper);
            presentation.updateCurrentSlideInModel();
        }
    }.bind(this);

    var setViewBox = function(paper)
    {
         paper.setViewBox(0, 0, parseInt(presentation.defaultValues().canvasWidth), parseInt(presentation.defaultValues().canvasHeight), true);
         paper.canvas.setAttribute('preserveAspectRatio', 'none');
    }

    var goToSlide= function(index, notUnselect){
        if (!notUnselect || notUnselect !== true)
        {
            presentation.unselectAll();
        }

        if (typeof index !== 'number')
        {
            index = presentation.slideStack.indexOf(index);
        }
        presentation.currentSlide(index);
        presentation.paper.clear();
        presentation.paper.serialize.load_json(presentation.slideStack()[presentation.currentSlide()].paperObject(), presentation.paper);
        //навесить события на элементы
        presentation.paper.forEach(function(item){
            if (item.prev)
                presentation.bindEvents(item);
            else
            {
                item.click(presentation.unselectAll);
            }
        });



    };

    var unselectAll = function(){
        presentation.paper.forEach(function(item){
            presentation.unselectItem(item);
        });
        presentation.updateCurrentSlideInModel();
        DOMMethods.showMainEditor();
    };

    var removeSelectedItem = function()
    {
        if (presentation.paper)
        {
            presentation.paper.forEach(function(item){
                if (item === presentation.selectedItem()) {
                    presentation.paper.getById(item.borderId).remove();
                    presentation.paper.getById(item.closerId).remove();
                    if (item.type === 'image')
                    {
                        presentation.paper.getById(item.resizerId).remove();
                    }
                    item.remove();
                    DOMMethods.showMainEditor();
                }
            });
            presentation.updateCurrentSlideInModel();
        }
    }



    var addText = function(){
        var text = presentation.paper.text(parseInt(presentation.defaultValues().canvasWidth)/2, parseInt(presentation.defaultValues().canvasHeight)/2,'Text')
            .attr('font-size',presentation.defaultValues().fontSize)
            .attr('font-family',presentation.defaultValues().fontFamily)
            .attr('fill',presentation.defaultValues().fontColor)
            .attr('font-weight',presentation.defaultValues().fontWeight)
            .attr('font-style',presentation.defaultValues().fontStyle)
            .attr('text-decoration',presentation.defaultValues().textDecoration)
            .attr('text-anchor','start');

        var rect = presentation.paper.rect(text.node.getBBox().x, text.node.getBBox().y, text.node.getBBox().width, text.node.getBBox().height)
                   .attr('stroke',presentation.defaultValues().fontColor);
        text.borderId = rect.id;

        var closer = presentation.paper.rect(text.node.getBBox().x+text.node.getBBox().width -10, text.node.getBBox().y, 10, 10)
            .attr('fill',presentation.defaultURLs.closer)
            .attr('stroke-width','1')
            .attr('cursor','pointer');
        text.closerId = closer.id;

        presentation.bindEvents(text);
        presentation.selectItem(text);
        presentation.updateCurrentSlideInModel();
    };


    var addImage = function(value)
    {
            var image = presentation.paper.image(value,0,0,200,200);

            var rect = presentation.paper.rect(image.node.getBBox().x, image.node.getBBox().y, image.node.getBBox().width, image.node.getBBox().height);
            image.borderId = rect.id;

            var closer = presentation.paper.rect(image.node.getBBox().x+image.node.getBBox().width -10, image.node.getBBox().y, 10, 10)
                .attr('fill',presentation.defaultURLs.closer)
                .attr('stroke-width','1')
                .attr('cursor','pointer');
            image.closerId = closer.id;

            var resizer = presentation.paper.rect(image.node.getBBox().x + image.node.getBBox().width -19, image.node.getBBox().y + image.node.getBBox().height -19, 20, 20)
                .attr('fill',presentation.defaultURLs.resizer)
                .attr('stroke-width','0')
                .attr('cursor','pointer');
            image.resizerId = resizer.id;

            presentation.bindEvents(image);
            presentation.selectItem(image);
            presentation.updateCurrentSlideInModel();
            DOMMethods.showMainEditor();
    }


    var selectItem = function(element) {

        if (!(element === presentation.selectedItem()))
        {

            presentation.paper.forEach(function(item){
                presentation.unselectItem(item);
            });
            presentation.showBorder(presentation.paper, element);
            presentation.showCloser(presentation.paper, element);
            if (element.type === 'image')
            {
                presentation.showResizer(presentation.paper, element);
            }
            presentation.selectedItem(element);
            if (element.type !== 'image')
            {
                DOMMethods.showElementEditor(element);
            }
            else
            {
                DOMMethods.showMainEditor();
            }
        }
    };

    var unselectItem = function(element) {
        presentation.selectedItem(undefined);
        presentation.hideBorder(presentation.paper, element);
        presentation.hideCloser(presentation.paper, element);
        if (element.type === 'image')
        {
            presentation.hideResizer(presentation.paper, element);
        }
    };


    var showCloser  = function(paper, element){
        paper.forEach(function(item){
            if (item.id === element.closerId)
            {
                //presentation.updateResizer(paper, element);
                item.attr('height','10');
            }
        });
    };

    var updateCloser = function(paper, element) {
        paper.forEach(function(item){
            if (item.id === element.closerId)
            {
                item.attr({x: element.node.getBBox().x + element.node.getBBox().width -10, y: element.node.getBBox().y , width: 10, height: 10 });
            }
        });

    };

    var hideCloser  = function(paper, element){
        paper.forEach(function(item){
            if (item.id === element.closerId)
            {
                item.attr('height','0');
            }
        });

    };


    var showResizer  = function(paper, element){
        paper.forEach(function(item){
            if (item.id === element.resizerId)
            {
                presentation.updateResizer(paper, element);
                item.attr('height','20');
            }
        });
    };

    var updateResizer = function(paper, element) {
        paper.forEach(function(item){
            if (item.id === element.resizerId)
            {
                item.attr({x: element.node.getBBox().x + element.node.getBBox().width -20, y: element.node.getBBox().y + element.node.getBBox().height - 20, width: 20, height: 20 });
            }
        });
    };

    var hideResizer  = function(paper, element){

        paper.forEach(function(item){
            if (item.id === element.resizerId)
            {
                presentation.updateResizer(paper, element);
                item.attr('height','0');
            }
        });

    };


    var showBorder  = function(paper, element){
        paper.forEach(function(item){
            if (item.id === element.borderId)
            {
                presentation.updateBorder(paper, element);
                item.attr('stroke-width','1');
            }
        });
    };

    var updateBorder = function(paper, element) {
        paper.forEach(function(item){
            if (item.id === element.borderId)
            {
                item.attr({x: element.node.getBBox().x, y: element.node.getBBox().y, width: element.node.getBBox().width, height: element.node.getBBox().height});
                if (element.type === 'text')
                {
                    item.attr('stroke', element.attrs.fill);
                }
            }
        });
    };

    var hideBorder  = function(paper, element){

        paper.forEach(function(item){
            if (item.id === element.borderId)
            {
                presentation.updateBorder(paper, element);
                item.attr('stroke-width','0');
            }
        });

    };




    var updateCurrentSlideInModel = function(){
        presentation.slideStack()[presentation.currentSlide()].paperObject(presentation.paper.serialize.json(presentation.paper));
        DOMMethods.setSaveButtonDefaultText();
    }.bind(this);

    var bindEvents = function(element) {
        if (element.type === 'text' || element.type === 'image')
        {
            var start = function () {
                    this.ox = this.attr("x");
                    this.oy = this.attr("y");
                    presentation.selectItem(this)

                },
                move = function (dx, dy) {
                    this.attr({x: this.ox + dx*(parseInt(presentation.defaultValues().canvasWidth)/parseInt(DOMElements.SVGWrapper.style.width)),
                        y: this.oy + dy*(parseInt(presentation.defaultValues().canvasHeight)/parseInt(DOMElements.SVGWrapper.style.height))});
                    presentation.updateBorder(presentation.paper, this);
                    presentation.updateCloser(presentation.paper, this);
                    if (this.type === 'image')
                    {
                        presentation.updateResizer(presentation.paper, this);
                    }
                },
                up = function () {
                    presentation.updateCurrentSlideInModel();
                };
            element.drag(move, start, up);

            element.click(function() {  presentation.selectItem(this)});

            presentation.paper.forEach(function(item){
                if (item.id === element.closerId)
                {
                    item.click(presentation.removeSelectedItem);
                }
            });
        }

        if (element.type === 'image')
        {
            presentation.paper.forEach(function(item){
                if (item.id === element.resizerId)
                {
                   var rstart = function () {

                       item.ox = item.attr("x");
                       item.oy = item.attr("y");
                       element.ow = element.attr("width");
                       element.oh = element.attr("height");
                   }.bind(this);

                   var rmove = function (dx, dy) {

                       item.attr({x: item.ox + dx*(parseInt(presentation.defaultValues().canvasWidth)/parseInt(DOMElements.SVGWrapper.style.width)),
                                  y: item.oy + dy*(parseInt(presentation.defaultValues().canvasHeight)/parseInt(DOMElements.SVGWrapper.style.height))});
                       element.attr({width: element.ow + dx*(parseInt(presentation.defaultValues().canvasWidth)/parseInt(DOMElements.SVGWrapper.style.width)),
                                     height: element.oh + dy*(parseInt(presentation.defaultValues().canvasHeight)/parseInt(DOMElements.SVGWrapper.style.height))});
                       presentation.updateBorder(presentation.paper, element);
                       presentation.updateCloser(presentation.paper, element);
                   }.bind(this);

                    item.drag(rmove, rstart,function(){presentation.updateCurrentSlideInModel()});
                }
            });
        }

    };


    var changeElementText = function()
    {

        presentation.selectedItem().attr('text',document.getElementsByClassName('edit-text-area')[0].value);
        presentation.updateBorder(presentation.paper, presentation.selectedItem());
        presentation.updateCloser(presentation.paper, presentation.selectedItem());
        presentation.updateCurrentSlideInModel();
    };

    var setSlideBackgroundColor = function(data)
    {
        var currentSlideModel = JSON.parse(presentation.slideStack()[presentation.currentSlide()].paperObject());
        currentSlideModel[0] = {
            type: "rect",
            id: currentSlideModel[0].id,
            x: 0,
            y: 0,
            transform: "",
            'stroke-width': 0,
            width: parseInt(presentation.defaultValues().canvasWidth)+2,
            height: parseInt(presentation.defaultValues().canvasHeight)+2,
            fill: data
        };
        presentation.slideStack()[presentation.currentSlide()].paperObject(JSON.stringify(currentSlideModel));
        presentation.goToSlide(presentation.currentSlide(),true);
        DOMMethods.hideSlideBackgroundColorPopup();
    };

    var setSlideBackgroundImage = function(data)
    {
        var currentSlideModel = JSON.parse(presentation.slideStack()[presentation.currentSlide()].paperObject());
        currentSlideModel[0] = {
            type: "image",
            id: currentSlideModel[0].id,
            x: 0,
            y: 0,
            transform: "",
            width: parseInt(presentation.defaultValues().canvasWidth)+2,
            height: parseInt(presentation.defaultValues().canvasHeight)+2,
            fill: '',
            src: data
        };
        presentation.slideStack()[presentation.currentSlide()].paperObject(JSON.stringify(currentSlideModel));
        presentation.goToSlide(presentation.currentSlide(),true);

    }

    var setCurrentBackgroundToAll = function()
    {
        var thisSlideBackground = JSON.parse(presentation.slideStack()[presentation.currentSlide()].paperObject())[0];

        presentation.slideStack().forEach(function(item){
           var currentSlideModel = JSON.parse(item.paperObject());
           currentSlideModel[0] = thisSlideBackground;
            item.paperObject(JSON.stringify(currentSlideModel));
        });
    }


    var resetSlideBackgroundToDefault = function()
    {
        var currentSlideModel = JSON.parse(presentation.slideStack()[presentation.currentSlide()].paperObject());

        if (presentation.defaultValues().background.type === "color"){
            currentSlideModel[0] = {
                type: "rect",
                id: currentSlideModel[0].id,
                x: 0,
                y: 0,
                transform: "",
                width: parseInt(presentation.defaultValues().canvasWidth),
                height: parseInt(presentation.defaultValues().canvasHeight),
                fill: presentation.defaultValues().background.value,
                'stroke-width': '0'
            };

        }
        else
        {
            currentSlideModel[0] = {
                type: "image",
                id: currentSlideModel[0].id,
                x: 0,
                y: 0,
                transform: "",
                width: parseInt(presentation.defaultValues().canvasWidth),
                height: parseInt(presentation.defaultValues().canvasHeight),
                src: presentation.defaultValues().background.value,
                'stroke-width': '0'
            };
        }

        presentation.slideStack()[presentation.currentSlide()].paperObject(JSON.stringify(currentSlideModel));
        presentation.goToSlide(presentation.currentSlide(),true);

    }

    var setSelectedItemFontFamily = function(data)
    {
        presentation.selectedItem(presentation.selectedItem().attr('font-family',data));
        presentation.updateBorder(presentation.paper, presentation.selectedItem());
        presentation.updateCloser(presentation.paper, presentation.selectedItem());
        presentation.updateCurrentSlideInModel();
    }

    var setSelectedItemFontSize = function(data)
    {
        presentation.selectedItem(presentation.selectedItem().attr('font-size',typeof data === "number" ? data :parseInt(DOMMethods.getFontSizeForItemValue() || presentation.defaultValues().fontSize)));
        presentation.updateBorder(presentation.paper, presentation.selectedItem());
        presentation.updateCloser(presentation.paper, presentation.selectedItem());
        presentation.updateCurrentSlideInModel();
    }

    var setSelectedItemColor = function(data)
    {
        presentation.selectedItem(presentation.selectedItem().attr('fill',data));
        presentation.updateBorder(presentation.paper, presentation.selectedItem());
        presentation.updateCloser(presentation.paper, presentation.selectedItem());
        presentation.updateCurrentSlideInModel();
    }

    var toogleSelectedItemBold = function()
    {
        presentation.selectedItem(presentation.selectedItem().attr('font-weight',DOMMethods.getItemBoldValue() ? 'bold' : 'normal'));
        presentation.updateBorder(presentation.paper, presentation.selectedItem());
        presentation.updateCloser(presentation.paper, presentation.selectedItem());
        presentation.updateCurrentSlideInModel();
    }

    var toogleSelectedItemItalic = function()
    {
        presentation.selectedItem(presentation.selectedItem().attr('font-style',DOMMethods.getItemItalicValue() ? 'italic' : 'normal'));
        presentation.updateBorder(presentation.paper, presentation.selectedItem());
        presentation.updateCloser(presentation.paper, presentation.selectedItem());
        presentation.updateCurrentSlideInModel();
    }

    var toogleSelectedItemUnderline = function()
    {
        presentation.selectedItem(presentation.selectedItem().attr('text-decoration',DOMMethods.getItemUnderlineValue() ? 'underline' : 'none'));
        presentation.updateBorder(presentation.paper, presentation.selectedItem());
        presentation.updateCloser(presentation.paper, presentation.selectedItem());
        presentation.updateCurrentSlideInModel();
    }

    var setPresentationFontFamily = function(data)
    {
        presentation.defaultValues().fontFamily = data;
        presentation.defaultValues(presentation.defaultValues());
    }

    var setPresentationFontSize = function(data)
    {
        presentation.defaultValues().fontSize = typeof data === "number" ? data :parseInt(DOMMethods.getFontSizeForPresentationValue() || presentation.defaultValues().fontSize);
        presentation.defaultValues(presentation.defaultValues());
    }

    var setPresentationFontColor = function(data)
    {
        presentation.defaultValues().fontColor = data;
        presentation.defaultValues(presentation.defaultValues());
    }

    var tooglePresentationBold = function()
    {
        presentation.defaultValues().fontWeight = (presentation.defaultValues().fontWeight === 'bold') ? 'normal' : 'bold';
        presentation.defaultValues(presentation.defaultValues());
    }

    var tooglePresentationItalic = function()
    {
        presentation.defaultValues().fontStyle = (presentation.defaultValues().fontStyle === 'italic') ? 'normal' : 'italic';
        presentation.defaultValues(presentation.defaultValues());
    }

    var tooglePresentationUnderline = function()
    {
        presentation.defaultValues().textDecoration = (presentation.defaultValues().textDecoration === 'underline') ? 'normal' : 'underline';
        presentation.defaultValues(presentation.defaultValues());
    }

    var setPresentationBackgroundColor = function(value)
    {
        presentation.defaultValues().background = { type: 'color', value: value};
        presentation.defaultValues(presentation.defaultValues());
    }

    var setPresentationBackgroundImage = function(value)
    {

        presentation.defaultValues().background = { type: 'image', value: value};
        presentation.defaultValues(presentation.defaultValues());
    }


    var savePresentation = function()
    {
        var resultObject = {
            name: DOMElements.presName.innerHTML,
            defaultValues: presentation.defaultValues(),
            slideStack: [],
            preview: presentation.slideStack()[0].previewImage(),
            private: DOMMethods.isPrivate()
        }

        presentation.slideStack().forEach(function(item)
        {
           resultObject.slideStack.push({ paperObject: item.paperObject(),
                                          id: item.id });
        });

        //send request

        DOMMethods.sendSaveRequest(resultObject);

    }


    var moveSlide = function(index, offset)
    {
        if ((Math.abs(offset) === 1) || (index > presentation.slideStack().length))
        {
            return;
        }
        else
        {
            var realOffset = offset >=0 ? offset -1 : offset + 1;
            if ((index + realOffset > presentation.slideStack().length) || (index + realOffset <= 0))
            {
                return;
            }
            presentation.slideStack(presentation.slideStack().move(index-1, index -1 + realOffset));
            presentation.goToSlide(index -1 + realOffset,true);
        }

    }

    return {
        slideStack: slideStack,
        defaultValues: defaultValues,
        setDefaultBackground: setDefaultBackground,
        selectedItem: selectedItem,
        initialize: initialize,
        Slide: Slide,
        setViewBox: setViewBox,
        addSlide: addSlide,
        removeSlide: removeSlide,
        bindEvents: bindEvents,
        goToSlide: goToSlide,
        updateCurrentSlideInModel: updateCurrentSlideInModel,
        currentSlide: currentSlide,
        addText: addText,
        addImage: addImage,
        paper: paper,
        fakePaper: fakePaper,
        showBorder: showBorder,
        hideBorder: hideBorder,
        updateBorder: updateBorder,
        showResizer: showResizer,
        updateResizer: updateResizer,
        hideResizer: hideResizer,
        showCloser: showCloser,
        updateCloser: updateCloser,
        hideCloser: hideCloser,
        selectItem: selectItem,
        removeSelectedItem: removeSelectedItem,
        unselectItem: unselectItem,
        unselectAll: unselectAll,
        changeElementText: changeElementText,
        colorsForPopups: colorsForPopups,
        fontFamiliesForPopups: fontFamiliesForPopups,
        fontSizesForPopups: fontSizesForPopups,
        setSlideBackgroundColor: setSlideBackgroundColor,
        setSlideBackgroundImage: setSlideBackgroundImage,
        setCurrentBackgroundToAll: setCurrentBackgroundToAll,
        resetSlideBackgroundToDefault: resetSlideBackgroundToDefault,
        setSelectedItemFontFamily: setSelectedItemFontFamily,
        setSelectedItemFontSize: setSelectedItemFontSize,
        setSelectedItemColor:setSelectedItemColor,
        toogleSelectedItemBold: toogleSelectedItemBold,
        toogleSelectedItemItalic: toogleSelectedItemItalic,
        toogleSelectedItemUnderline: toogleSelectedItemUnderline,
        setPresentationFontFamily: setPresentationFontFamily,
        setPresentationFontSize: setPresentationFontSize,
        setPresentationFontColor: setPresentationFontColor,
        tooglePresentationBold: tooglePresentationBold,
        tooglePresentationItalic: tooglePresentationItalic,
        tooglePresentationUnderline: tooglePresentationUnderline,
        setPresentationBackgroundColor: setPresentationBackgroundColor,
        setPresentationBackgroundImage: setPresentationBackgroundImage,
        savePresentation: savePresentation,
        defaultURLs: defaultURLs,
        moveSlide: moveSlide
    };


})();

