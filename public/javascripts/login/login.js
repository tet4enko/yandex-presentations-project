/**
 * Created by Dmitriy on 07.03.14.
 */

var DOMElements = {
    mailInput : document.getElementById('email'),
    passInput : document.getElementById('pass'),
    mailError : document.getElementsByClassName('form-row mail')[0].getElementsByClassName('error')[0],
    passError : document.getElementsByClassName('form-row password')[0].getElementsByClassName('error')[0],
    submitButton : document.getElementsByClassName('submit')[0],
    tryButton : document.getElementsByClassName('try')[0],
    regButton : document.getElementsByClassName('signUp')[0],

    notFoundError : document.getElementsByClassName('global-error red')[0]
}

var DOMMethods = {
    showMailError : function()
    {
        DOMElements.mailError.innerHTML = '(Enter a not-empty e-mail)';
        DOMElements.mailInput.className = 'error';
    },

    hideMailError : function()
    {
        DOMElements.mailError.innerHTML = '';
        DOMElements.mailInput.className = 'success';
    },

    showPasswordError : function()
    {
        DOMElements.passError.innerHTML = '(Enter a not-empty password)';
        DOMElements.passInput.className = 'error';
    },

    hidePasswordError : function()
    {
        DOMElements.passError.innerHTML = '';
        DOMElements.passInput.className = 'success';
    },

    showNotFoundError : function()
    {
        DOMElements.notFoundError.innerHTML = 'Incorrect e-mail or password !'
    },

    submitForm : function()
    {
        var flag = 1;

        if (!validationObject.checkMail(DOMElements.mailInput.value))
        {
            DOMMethods.showMailError();
            flag = 0;
        }
        else
        {
            DOMMethods.hideMailError();
        }

        if (!validationObject.checkPass(DOMElements.passInput.value))
        {
            DOMMethods.showPasswordError();
            flag = 0;
        }
        else
        {
            DOMMethods.hidePasswordError();
        }

        if (flag === 1)
        {
            //send request

            requestController.sendRequest('POST', window.location.pathname, 'data='+JSON.stringify({
                mail : DOMElements.mailInput.value,
                pass : DOMElements.passInput.value
            }), function(e){
                if (e.target.readyState != 4) return;
                if (e.target.responseText === 'notfound')
                {
                    DOMMethods.showNotFoundError();
                }
                else
                {
                    window.location.href = '/user/' + e.target.responseText;
                }
            })

        }
    },

    createPublicPresentation: function()
    {
            requestController.sendRequest('POST', '/trynowpresentation', 'data=createUserPres', function(e) {
            if (e.target.readyState != 4) return;
            window.location.href = '/presentation/' + e.target.responseText;
        });
    }
}

var validationObject = {

    mailRegexp : /^[-a-z0-9!#$%&'*+/=?^_`{|}~]+(?:\.[-a-z0-9!#$%&'*+/=?^_`{|}~]+)*@(?:[a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\.)*(?:aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])$/,

    checkMail : function(value)
    {
        return (value.length > 0);
    },

    checkPass : function(value)
    {
        return (value.length > 0);
    }
}


DOMElements.mailInput.addEventListener('keyup',function(e)
{
    if (validationObject.checkMail(e.target.value))
    {
        DOMMethods.hideMailError();
    }
    else
    {
        DOMMethods.showMailError();
    }
});


DOMElements.passInput.addEventListener('keyup',function(e)
{
    if (validationObject.checkPass(e.target.value))
    {
        DOMMethods.hidePasswordError();
    }
    else
    {
        DOMMethods.showPasswordError()
    }
});

document.body.addEventListener('click',function(event)
{
    if (event.target.className === 'btn submit') DOMMethods.submitForm();
    if (event.target.className === 'btn sign-up') window.location.href = '/registration';
    if (event.target.className === 'btn btn-ok try') DOMMethods.createPublicPresentation();
});

document.body.addEventListener('keyup', function(event){
    if (event.keyCode === 13)
    {
        DOMMethods.submitForm();
    }
});

