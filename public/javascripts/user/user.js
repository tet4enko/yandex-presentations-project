/**
 * Created by Dmitriy on 09.03.14.
 */

var DOMElements =
{
    addImageInput : document.getElementsByClassName('photo-input').length ? document.getElementsByClassName('photo-input')[0] : undefined,
    photo: document.getElementsByClassName('photo')[0],
    addImageLoading :  document.getElementsByClassName('loading').length ? document.getElementsByClassName('loading')[0] : undefined,
    presentations : document.getElementsByClassName('presentation-row'),
    categories : document.getElementsByClassName('search-cat'),
    searchInput : document.getElementsByClassName('search-input')[0],
    searchUserInput : document.getElementsByClassName('search-user-input')[0],
    searchUserTimeoutId : '',
    searchUserPopup : document.getElementsByClassName('search-user')[0].getElementsByClassName('popup')[0],
    searchUserBlock :document.getElementsByClassName('search-user')[0]
}

var UIVariables = {
    searchUserStatus : 0
}


var DOMMethods =
{
    getAttr: function(ele, attr) {
        var result = ele.getAttribute(attr) || ele[attr] || null;
        if( !result ) {
            var attrs = ele.attributes;
            var length = attrs.length;
            for(var i = 0; i < length; i++)
                if(attr[i].nodeName === attr)
                    result = attr[i].nodeValue;
        }
        return result;
    },

    logOff : function()
    {
        requestController.sendRequest('POST', '/logoff','',function(e) {
            if (e.target.readyState != 4) return;
            if (e.target.responseText === 'success')
            {
                window.location.href = "/";
            }
        });
    },

    createUserPresentation : function()
    {
        requestController.sendRequest('POST', window.location.pathname, 'data=createUserPres', function(e) {
            if (e.target.readyState != 4) return;
            window.location.href = '/presentation/' + e.target.responseText;
        });
    },

    openPhotoInput: function()
    {
        if (DOMElements.addImageInput)
        {
            DOMElements.addImageInput.click();
        }
    },

    setPhoto: function(url)
    {
        DOMElements.photo.style.background = "url('" + url + "')";
        DOMElements.photo.style.backgroundSize = "100% 100%";
    },

    showPhotoLoading: function()
    {
        if (DOMElements.addImageLoading.className.indexOf('showed') < 0)
        {
            DOMElements.addImageLoading.className+= ' showed';
        }
    },

    hidePhotoLoading: function()
    {
        DOMElements.addImageLoading.className = DOMElements.addImageLoading.className.replace(' showed','');
    },

    selectCategory: function(target)
    {
        //DOMElements.categories.forEach(function(item)
        for (var i=0; i<DOMElements.categories.length; i++)
        {
            var item = DOMElements.categories[i];
            item.className = item.className.replace(' selected', '');
        };
        target.className += ' selected';


        if (target.className.indexOf('private')>=0)
        {
            for (var i=0; i<DOMElements.presentations.length; i++)
            {
                var item = DOMElements.presentations[i];
                item.className = (DOMMethods.getAttr(item,'data-id') === 'private') ? item.className.replace(' hided-by-filter','') : item.className + ' hided-by-filter';
            };
        }
        else
        if (target.className.indexOf('public')>=0)
        {
            for (var i=0; i<DOMElements.presentations.length; i++)
            {
                var item = DOMElements.presentations[i];
                item.className = (DOMMethods.getAttr(item,'data-id') === 'public') ? item.className.replace(' hided-by-filter','') : item.className + ' hided-by-filter';
            };
        }
        else
        {
            for (var i=0; i<DOMElements.presentations.length; i++)
            {
                var item = DOMElements.presentations[i];
                item.className = item.className.replace(' hided-by-filter','');
            };
        }

    },

    hideSearchUserPopup: function()
    {
        if (DOMElements.searchUserPopup.className.indexOf('showed') >= 0)
        {
            DOMElements.searchUserPopup.className = DOMElements.searchUserPopup.className.replace(' showed','');
        }
    },

    showSearchUserPopup: function()
    {
        if (DOMElements.searchUserPopup.className.indexOf('showed') < 0)
        {
            DOMElements.searchUserPopup.className += ' showed';
        }

    },

    showSearchUserResults: function(results)
    {
        var innerHtml = '';
        if (results === 'none')
        {
            innerHtml += "<a class='popup-row sans-serif pointer'> <div class='popup-row-photo'></div>  <span class='popup-row-caption'>No results...</span> </a>";
            UIVariables.searchUserStatus = 1;
        }
        else
        {
            var tooLong = results.length>6;
            results.slice(0,6).forEach(function(item){
                innerHtml +=  "<a class='popup-row sans-serif pointer' href='/user/" + item.id + "'> <div class='popup-row-photo' style=\"background: url(\'" + item.photo + "\')\; background-size:100%100%\"></div>  <span class='popup-row-caption'>" + item.firstName + " " + item.lastName + "</span> </a>";
            });
            if (tooLong)
            {
                innerHtml += "<div class='popup-row sans-serif pointer'> <span class='popup-row-caption'> Too much results... </span> </div>";
            }

            UIVariables.searchUserStatus = 2;
        }

        DOMElements.searchUserPopup.innerHTML = innerHtml;
        DOMMethods.showSearchUserPopup();

    },

    deletePresentation: function(target)
    {
        requestController.sendRequest('POST', '/deletepresentation',"presId=" + DOMMethods.getAttr(target,'data-id'), function(e) {
            if (e.target.readyState != 4) return;
            if (e.target.responseText === 'success')
            {
                this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode); //IE...
            }
        }.bind(target));
    },

    forkPresentation: function(target)
    {
        requestController.sendRequest('POST', '/forkpresentation', "presId=" + DOMMethods.getAttr(target,'data-id'), function(e) {
            if (e.target.readyState != 4) return;
            if (e.target.responseText === 'success')
            {
                target.className = 'forked sans-serif darkgray';
                target.innerHTML = 'Forked!'
            }
        }.bind(target));
    },

    tryGetUpdate: function(target)
    {
        requestController.sendRequest('POST', '/updatepresentation', "presId=" + DOMMethods.getAttr(target,'data-id'), function(e) {
            if (e.target.readyState != 4) return;

            if (e.target.responseText === 'nothingtoupdate')
            {
                this.innerHTML = 'Nothing to Update!'
            }
            else if (e.target.responseText === 'success')
            {
                this.innerHTML = 'Updated succesfully!'
            }
        }.bind(target));
    },

    createPullRequest: function(target)
    {
        requestController.sendRequest('POST', '/createpullrequest', "presId=" + DOMMethods.getAttr(target,'data-id'), function(e) {
            if (e.target.readyState != 4) return;
            if (e.target.responseText === 'success')
            {
                this.innerHTML = 'Succesfully!'
            }
        }.bind(target));
    },

    createPullRequestPresentation: function(target)
    {
        requestController.sendRequest('POST', '/createpullrequestpres', "pullreqId=" + DOMMethods.getAttr(target,'data-id'), function(e) {
            if (e.target.readyState != 4) return;
            if (e.target.responseText !== 'error')
            {
                window.location.href = "/presentation/" + e.target.responseText;
            }
        }.bind(target));
    },

    acceptPullRequest: function(target)
    {
        requestController.sendRequest('POST', '/acceptpullrequest', "pullreqId=" + DOMMethods.getAttr(target,'data-id'), function(e) {
            if (e.target.readyState != 4) return;
            if (e.target.responseText === 'success')
            {
                this.parentNode.innerHTML = "<span class='green'>Accepted!</span>"
            }
        }.bind(target));
    },

    declinePullRequest: function(target)
    {
        requestController.sendRequest('POST', '/declinepullrequest', "pullreqId=" + DOMMethods.getAttr(target,'data-id'), function(e) {
            if (e.target.readyState != 4) return;

            if (e.target.responseText === 'success')
            {
                this.parentNode.innerHTML = "<span class='red'>Declined!</span>"
            }
        }.bind(target));
    }


}

function isDescendant(parent, child) {
    var node = child.parentNode;
    while (node != null) {
        if (node == parent) {
            return true;
        }
        node = node.parentNode;
    }
    return false;
}


document.body.addEventListener('click', function(e){
    if (e.target.className.indexOf('btn btn-ok log-off') >= 0) DOMMethods.logOff();
    if (e.target.className.indexOf('btn log-in') >= 0) window.location.href = '/';;
    if (e.target.className.indexOf('btn btn-ok sign-up') >= 0) window.location.href = '/registration';
    if (e.target.className.indexOf('btn create') >= 0) DOMMethods.createUserPresentation();
    if (e.target.className.indexOf('photo') >= 0 || e.target.className.indexOf('add-photo') >= 0 ) DOMMethods.openPhotoInput();
    if (e.target.className.indexOf('search-cat') >= 0) DOMMethods.selectCategory(e.target);
    if (e.target.className.indexOf('presentation-delete') >= 0) DOMMethods.deletePresentation(e.target);
    if (e.target.className.indexOf('presentation-fork') >= 0) DOMMethods.forkPresentation(e.target);
    if (e.target.className.indexOf('action update') >= 0) DOMMethods.tryGetUpdate(e.target);
    if (e.target.className.indexOf('action pullrequest') >= 0) DOMMethods.createPullRequest(e.target);
    if (e.target.className.indexOf('see') >= 0) DOMMethods.createPullRequestPresentation(e.target);
    if (e.target.className.indexOf('accept') >= 0) DOMMethods.acceptPullRequest(e.target);
    if (e.target.className.indexOf('decline') >= 0) DOMMethods.declinePullRequest(e.target);
    //search user popup
    if (!isDescendant(DOMElements.searchUserBlock,e.target)) DOMMethods.hideSearchUserPopup();
    if ((e.target.className.indexOf('search-user-input') >= 0) && (UIVariables.searchUserStatus>0) && (e.target.value != "")) DOMMethods.showSearchUserPopup();

});

if (DOMElements.addImageInput)
{
    DOMElements.addImageInput.addEventListener('change',function(e){
        if (e.target.files[0])
        {
            var formData = new FormData();
            formData.append("file", e.target.files[0]);
            formData.append("userId", window.location.pathname.replace('\/user\/',''));
            var xhr = new XMLHttpRequest();

            xhr.open("POST", "/saveimage");

            xhr.onreadystatechange = function(e) {
                if (xhr.readyState != 4) return;
                DOMMethods.setPhoto(e.target.responseText.replace("public\\images\\","/public/images/"));
                DOMMethods.hidePhotoLoading();
            }
            DOMMethods.showPhotoLoading();

            xhr.send(formData);

            this.value = '';
        }
    });
}

DOMElements.searchInput.addEventListener('keyup',function(e){
    var value = e.target.value;
    var presentationsToSearch = [];
    for (var i=0; i<DOMElements.presentations.length; i++)
    {
        if (DOMElements.presentations[i].className.indexOf('hided-by-filter') < 0)
        {
            presentationsToSearch.push(DOMElements.presentations[i]);
        }
    }

    presentationsToSearch.forEach(function(item){
        item.className = (item.getElementsByClassName('presentation-href')[0].innerHTML.indexOf(value) < 0) ?
            ((item.className.indexOf(' hided-by-search') < 0) ? item.className + ' hided-by-search' : item.className ) :
            item.className.replace(/ hided-by-search/gi,'');
    });


});

DOMElements.searchUserInput.addEventListener('keyup',function(e){
    if (DOMElements.searchUserTimeoutId)
    {
        clearTimeout(DOMElements.searchUserTimeoutId);
    }
    DOMElements.searchUserTimeoutId = setTimeout(function(){

        requestController.sendRequest('POST', '/searchuser', 'searchUser=' + e.target.value, function(e) {
            if (e.target.readyState != 4) return;
            DOMMethods.showSearchUserResults(e.target.responseText === 'none' ? 'none' : JSON.parse(e.target.responseText));
        });

    },500);

});



//DOMElements.searchUserInput.addEventListener('blur',DOMMethods.hideSearchUserPopup);