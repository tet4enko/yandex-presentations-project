/**
 * Created by Dmitriy on 08.03.14.
 */

var User = require('../models/user').User;
var UAParser = require('ua-parser-js');

exports.get = function(req, res){
    var parser = new UAParser();
    var ua = req.headers['user-agent'];
    var browserName = parser.setUA(ua).getBrowser().name;
    res.render('registration.ejs',  { cssFile : browserName === "IE" ? "IEstyle.css" : "style.css"  });
};

exports.post = function(req, res){
    var inputObject = JSON.parse(req.body.data);

    var user = new User(
        {
            firstName: inputObject.firstName,
            lastName: inputObject.lastName,
            mail: inputObject.email,
            password: inputObject.password
        }
    );

    user.save(function(err, user, affected)
    {
        if (err) {
            res.send('dublicate');
        }
        else
        {
            req.session.userId = user.get('id');
            res.send(user.get('id'));
        }

    });

};