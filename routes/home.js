/**
 * Created by Dmitriy on 25.01.14.
 */
//var fs = require('fs');
//var formidable = require('formidable');
//var async = require('async');
var User = require('../models/user').User;
var UAParser = require('ua-parser-js');

exports.get = function(req, res){
    var parser = new UAParser();
    var ua = req.headers['user-agent'];
    var browserName = parser.setUA(ua).getBrowser().name;
 /*   var fullBrowserVersion = parser.setUA(ua).getBrowser().version;
    var browserVersion = fullBrowserVersion.split(".",1).toString();
    var browserVersionNumber = Number(browserVersion);*/

    if (!req.session.userId)
    {
        res.render('login.ejs', { cssFile : browserName === "IE" ? "IEstyle.css" : "style.css"  });
    }
    else
    {
        res.redirect('/user/' + req.session.userId);
    }

};

exports.post = function(req, res){
    var inputObject = JSON.parse(req.body.data);
    User.findOne({mail: inputObject.mail},function(err, user){
        if (user && user.checkPassword(inputObject.pass))
        {
            req.session.userId = user.get('id');
            res.send(user.get('id'));
        }
        else
        {
            res.send('notfound');
        }
    });

};

