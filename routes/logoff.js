/**
 * Created by Dmitriy on 09.03.14.
 */

exports.post = function(req, res){
    req.session.destroy();
    res.send('success');
};