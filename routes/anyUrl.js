/**
 * Created by Dmitriy on 16.03.14.
 */

exports.get = function(req, res)
{
    res.render('error.ejs', {message: 'Page not Found'});
}