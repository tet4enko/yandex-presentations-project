/**
 * Created by Dmitriy on 09.03.14.
 */

var async = require('async');
var User = require('../models/user').User;
var Presentation = require('../models/presentation').Presentation;
var PullRequest = require('../models/pullrequest').PullRequest;
var UAParser = require('ua-parser-js');
var presentationUtility = require('../business/presentationUtility');

exports.get = function(req, res)
{
    async.waterfall([
        function(callback)
        {
            if (req.session.userId)
            {
                User.findOne({_id : req.session.userId }, function(err, user) {
                    callback(err,user.toSimpleObject());
                }.bind(this))
            }
            else
            {
                callback(null,null);
            }
        },
        function(user, callback)
        {
            Presentation.findOne({_id : req.params.presId}, function(err, pres){
                callback(err, user, pres ? pres.toSimpleObject() : null)
            }.bind(this));
        },
        function(currentUser, presentation, callback)
        {
            if (!presentation)
            {
                callback(null, currentUser, null, null);
            }
            else if (presentation.userId === 'temp' || presentation.userId === 'pullrequest')
            {
                callback(null, currentUser, presentation, presentation.userId);
            }
            else
            {
                User.findOne({_id : presentation.userId }, function(err, presentationUser) {
                    callback(err,currentUser, presentation, presentationUser ? (presentationUser !== 'temp' ? presentationUser.toSimpleObject() : 'temp') : null);
                }.bind(this));
            }
        }

    ], function(err,currentUser, presentation, presentationUser){
        if (err || !presentation)
        {
            res.render('error.ejs', {message: 'Presentation not Found'});
        }
        else if (presentation.private && (!currentUser || (currentUser.id !== presentation.userId)))
        {
            res.render('error.ejs', {message: 'It\'s Private Presentation'});
        }
        else
        {
            var parser = new UAParser();
            var ua = req.headers['user-agent'];
            var browserName = parser.setUA(ua).getBrowser().name;

            res.render('builder.ejs',{presentation : presentation,
                                      your: (currentUser && (currentUser.id === presentation.userId)),
                                      presentationUser : presentationUser,
                                      cssFile : browserName === "IE" ? "BuilderIE.css" : "builder.css"});
        }
    });

};


exports.post = function(req, res)
{
    if (!req.body.name)
    {
        Presentation.findOne({_id : req.params.presId},function(err, pres){
            if (err) res.send('error');
            if (!pres) res.send('presentation not found');
            res.send(pres.get('body'));

        });
    }
    else
    {
        Presentation.findOne({_id : req.params.presId},function(err, pres){
            if (err || (!pres) || (pres.get('userId') === 'pullrequest') || (!req.session.userId && (pres.get('userId') !== 'temp')) ||
                (req.session.userId && req.session.userId != pres.get('userId')))
            {
                res.send('error');
            }
            else
            {
                pres.set('name', req.body.name);
                pres.set('private', req.body.private);
                pres.set('body', req.body.body);
                pres.set('preview', req.body.preview);
                pres.set('updated', new Date().toLocaleDateString());

                pres.save(function(err, pres){
                    if (err)
                    {
                        res.send('error');
                    }
                    else
                    {
                        res.send('success');
                    }
                });

            }

        });
    }
}

exports.delete = function(req, res)
{
    if (!req.session.userId || !req.body.presId)
    {
        res.send('error');
    }
    else
    {
        Presentation.findOne({ _id : req.body.presId}, function(err, presentation){
            if (err || !presentation || ( presentation.get('userId') !== req.session.userId))
            {
                res.send('error');
            }
            else
            {

                async.waterfall([
                    function(callback){
                        if (presentation.forked.presId)
                        {
                            Presentation.findOne({_id : presentation.forked.presId}, function(err, pres){
                                callback(err, pres);
                            });
                        }
                        else
                        {
                            callback(null, null);
                        }
                    },
                    function(parentPres, callback)
                    {
                        if (parentPres)
                        {
                            parentPres.forks.splice(parentPres.forks.indexOf(presentation.id), 1);
                            parentPres.save(function(err){
                                callback(err);
                            });
                        }
                        else
                        {
                            callback(null);
                        }
                    }
                ],
                    function(err){
                    if (err)
                    {
                        res.send('error');
                    }
                    else
                    {
                        presentation.remove();
                        res.send('success');
                    }
                })


            }
        });
    }
}

exports.fork = function(req, res)
{
    if (!req.session.userId || !req.body.presId)
    {
        res.send('error');
    }
    else
    {

        async.waterfall([
            function(callback)
            {
                Presentation.findOne({ _id : req.body.presId}, function(err, presentation){
                    if (err ||
                        !presentation ||
                        (!err && presentation && presentation.get('userId') === req.session.userId) ||
                        (presentation.forks.indexOf(req.session.userId) >= 0))
                    {
                        callback('error');
                    }
                    else
                    {
                        presentation.forks.push(req.session.userId);
                        callback(err, presentation);
                    }
                });
            },
            function(presentation, callback)
            {
                User.findOne({ _id : presentation.userId}, function(err, user){
                    if (!user)
                    {
                        callback('error');
                    }
                    else
                    {
                        callback(err, user, presentation);
                    }
                });
            },
            function(presentationUser, presentation, callback)
            {
                presentation.save(function(err, pres){
                    callback(err, presentationUser, presentation);
                });
            },
            function(presentationUser, presentation, callback)
            {
                var forkedPresentation = new Presentation(
                    {
                        name: presentation.name,
                        private: false,
                        body: presentation.body,
                        preview: presentation.preview,
                        userId: req.session.userId,
                        forks: [],
                        forked: {
                            userId : presentation.userId,
                            userName : presentationUser.firstName + ' ' + presentationUser.lastName,
                            presId : presentation.id,
                            presName : presentation.name
                        }
                    }
                );

                forkedPresentation.save(function(err, pres){
                    callback(err);
                });
            }
        ],
            function(err){
                res.send(err ? 'error' : 'success');
        });



    }
}


exports.update = function(req, res)
{
    if (!req.session.userId || !req.body.presId)
    {
        res.send('error');
    }
    else
    {
        async.waterfall([
            function(callback)
            {
                Presentation.findOne({ _id : req.body.presId}, function(err, presentation){
                    if (err ||
                        !presentation ||
                        (presentation.get('userId') !== req.session.userId) ||
                        (!presentation.forked.presId))
                    {
                        callback('error');
                    }
                    else
                    {
                        callback(err, presentation);
                    }
                });
            },
            function(presentationToUpdate, callback)
            {
                Presentation.findOne({ _id : presentationToUpdate.forked.presId}, function(err, presentation){
                    if (err || !presentation)
                    {
                        callback('error');
                    }
                    else
                    {
                        var updateStatus = presentationUtility.update(presentation, presentationToUpdate);
                        if (!updateStatus)
                        {
                            res.send('nothingtoupdate');
                        }
                        else
                        {
                            presentationToUpdate.save(function(err){
                                callback(err);
                            });
                        }
                    }
                });
            }
        ],
            function(err){
            res.send(err ? 'error' : 'success');
        });
    }
}



exports.createPullRequest = function(req, res)
{
    if (!req.session.userId || !req.body.presId)
    {
        res.send('error');
    }
    else
    {
        async.waterfall([
            function(callback)
            {
                Presentation.findOne({ _id : req.body.presId}, function(err, presentation){
                    if (err ||
                        !presentation ||
                        (presentation.get('userId') !== req.session.userId) ||
                        (!presentation.forked.presId))
                    {
                        callback('error');
                    }
                    else
                    {
                        callback(err, presentation);
                    }
                });
            },
            function(presentation, callback)
            {
                User.findOne({_id : req.session.userId}, function(err, user){
                    if (!user)
                    {
                        callback('error');
                    }
                    else
                    {
                        callback(err, presentation, user.toSimpleObject());
                    }
                })  ;
            },
            function(presentation, user, callback)
            {
                var pullRequest = new PullRequest(
                    {
                        body : presentation.body,
                        presentationFromId : presentation.id,
                        presentationToId : presentation.forked.presId,
                        userToId : presentation.forked.userId,
                        userFromId : presentation.userId,
                        userFromName: user.firstName + ' ' + user.lastName,
                        presentationToName : presentation.name
                    }
                );

                pullRequest.save(function(err){
                    callback(err);
                });
            }
        ],
            function(err)
            {
                res.send(err ? 'error' : 'success');
            });
    }
}




exports.createPullRequestPresentation = function(req, res)
{
    async.waterfall([
        function(callback)
        {
            if (!req.body.pullreqId)
            {
                callback('error');
            }
            else
            {
                PullRequest.findOne({_id : req.body.pullreqId}, function(err,pullrequest){
                   if (!pullrequest)
                   {
                       callback('error');
                   }
                    else
                   {
                       callback(err, pullrequest);
                   }
                });
            }

        },
        function(pullrequest, callback)
        {
                Presentation.findOne({_id: pullrequest.presentationToId}, function(err, presentation){
                    if (!presentation)
                    {
                        callback('error');
                    }
                    else
                    {
                        callback(err, pullrequest, presentation);
                    }
                });
        },
        function(pullrequest, presentationTo, callback)
        {
            var pullRequestPresentation = new Presentation(
                {
                    name: presentationTo.name,
                    private: presentationTo.private,
                    body: presentationUtility.getUpdatedBody(pullrequest.body, presentationTo.body),
                    userId: 'pullrequest'
                }
            );


            pullRequestPresentation.save(function(err){
                callback(err, pullRequestPresentation);
            });
        }

    ],
        function(err, pullRequestPresentation){
            res.send(err ? 'error' : pullRequestPresentation.id);
        });
}


exports.acceptPullRequest = function(req, res)
{
    async.waterfall([
        function(callback)
        {
            if (!req.body.pullreqId || !req.session.userId)
            {
                callback('error');
            }
            else
            {
                PullRequest.findOne({_id : req.body.pullreqId}, function(err,pullrequest){
                    if (!pullrequest)
                    {
                        callback('error');
                    }
                    else
                    {
                        callback(err, pullrequest);
                    }
                });
            }
        },
        function(pullrequest, callback)
        {
            Presentation.findOne({_id: pullrequest.presentationToId}, function(err, presentation){
                if (!presentation || presentation.userId !== req.session.userId)
                {
                    callback('error');
                }
                else
                {
                    callback(err, pullrequest, presentation);
                }
            });
        },
        function(pullrequest, presentationTo, callback)
        {
            presentationTo.body = presentationUtility.getUpdatedBody(pullrequest.body, presentationTo.body);
            pullrequest.remove();

            presentationTo.save(function(err)
            {
                callback(err);
            });
        }
    ],
        function(err){
            res.send(err ? 'error' : 'success');
        });
}


exports.declinePullRequest = function(req, res)
{
    async.waterfall([
        function(callback)
        {
            if (!req.body.pullreqId || !req.session.userId)
            {
                callback('error');
            }
            else
            {
                PullRequest.findOne({_id : req.body.pullreqId}, function(err,pullrequest){
                    if (!pullrequest)
                    {
                        callback('error');
                    }
                    else
                    {
                        pullrequest.remove();
                        callback(err);
                    }
                });
            }
        }

    ],
        function(err){
            res.send(err ? 'error' : 'success');
        });
}


exports.createTryNowPresentation = function(req, res)
{
    var presentation = new Presentation(
        {
            name: 'New Presentation',
            private: false,
            body: '',
            userId: 'temp'
        }
    );
    presentation.save(function(err, presentation)
    {
        if (err) {
            res.send('error');
        }
        else
        {
            res.send(presentation.get('id'));
        }

    });
}