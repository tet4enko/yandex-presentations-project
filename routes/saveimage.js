/**
 * Created by Dmitriy on 10.03.14.
 */
var fs = require('fs');
var formidable = require('formidable');
var User = require('../models/user').User;
var async = require('async');

exports.post = function(req, res){

    form = new formidable.IncomingForm();
    form.uploadDir = 'public/images';

    form.parse(req, function(err, fields, files) {
        if (fields.userId){
            if (!req.session.userId || (req.session.userId !== fields.userId))
            {
                res.end('error');
            }
            else
            {
                async.waterfall([
                    function(callback)
                    {
                        User.findOne({_id : fields.userId},function(err, user){
                            callback(err, user);
                        });
                    },
                    function(user, callback)
                    {
                        user.set('photo', files.file.path.replace("public\\images\\","/public/images/"));
                        user.save(callback)
                    }
                ],function(err){
                    if (err) req.end('error');
                    res.end(files.file.path);
                });

            }

        }
        else
        {
            res.end(files.file.path);
        }
    }.bind(this));

}