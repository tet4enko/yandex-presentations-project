

module.exports = function(app) {

    app.get('/', require('./home').get);
    app.post('/', require('./home').post);

    app.get('/registration', require('./registration').get);
    app.post('/registration', require('./registration').post);

    app.post('/logoff', require('./logoff').post);

    app.get('/user/:userId', require('./user').get)
    app.post('/user/:userId', require('./user').createUserPresentation)
    app.post('/searchuser', require('./user').searchUser)

    app.get('/presentation/:presId', require('./presentation').get)
    app.post('/presentation/:presId', require('./presentation').post)
    app.post('/deletepresentation', require('./presentation').delete)
    app.post('/forkpresentation', require('./presentation').fork)
    app.post('/updatepresentation', require('./presentation').update)
    app.post('/createpullrequest', require('./presentation').createPullRequest)
    app.post('/createpullrequestpres', require('./presentation').createPullRequestPresentation)
    app.post('/acceptpullrequest', require('./presentation').acceptPullRequest)
    app.post('/declinepullrequest', require('./presentation').declinePullRequest)

    app.post('/trynowpresentation', require('./presentation').createTryNowPresentation);

    app.post('/saveimage', require('./saveimage').post)

    app.get('/:anyUrl', require('./anyUrl').get);

};
